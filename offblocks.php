<?php

/*
 * Plugin Name: #OffBlocks
 * Description: Provide functionality required for the website
 * Author: Sunil Jaiswal
 * Version: 1.0
 */

namespace OffBlocks;

require 'vendor/autoload.php';

//error_reporting(E_ERROR | E_WARNING | E_PARSE);

// In use
Airports::Setup();
Cities::Setup();
Plugin::Setup();
Shortcodes::Setup();
View::Setup();
Notams::Setup();
Geo::Setup();
MetarTaff::Setup();

