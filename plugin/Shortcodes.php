<?php

namespace OffBlocks;

class Shortcodes
{

	public static function Setup()
	{
		add_shortcode('tm', __CLASS__ . '::tm');
		add_shortcode('h', __CLASS__ . '::h');
		add_shortcode('investing_years',__CLASS__ . '::investingYears');
		add_shortcode('teaching_years',__CLASS__ . '::teachingYears');
		add_shortcode('required', __CLASS__ . '::required');
		add_shortcode('helper', __CLASS__ . '::helper');

	}

	public static function helper($attributes)
	{
		$a = (object)shortcode_atts(
			[
				'title' => 'Help',
				'text'  => '',
				'class' => 'darkGreen',
			], $attributes
		);

		if (!$a->content) $a->content = $attributes[0];

		return '<span class="darkGreen">[extra title="' . $a->title . '" info="popover" info_place="top" info_trigger="hover" info_content="' . $a->text . '" ][x_icon class="' . $a->class . '" type="question-circle"][/extra]</span>';

	}

	public static function required()
	{
		return '<span style="color:#CC0000"> *</span>';
	}

	public static function investingYears()
	{
		return date("Y") - 1999;
	}

	public static function teachingYears()
	{
		return date("Y") - 2002;
	}

	public static function h($attributes, $content)
	{
		return '<span style="color:#DD1C1A">' . $content . '</span>';
	}

	public static function tm()
	{
		return '&#8482;';
	}

	

}