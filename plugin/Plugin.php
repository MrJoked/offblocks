<?php

namespace OffBlocks;

use Mailgun\Mailgun as Mailgun;

class Plugin
{
	const timezonedb_key__ = "HWNYMBVEVNB0";
	const timezone_rqst_str= "http://api.timezonedb.com/v2/get-time-zone";
	const google_api_key__ 		 = "AIzaSyAF0N3AqZpy25gKyVPqayaEswt749qPJ4M";

	public static function Setup()
	{

		// Create actions
		add_action('wp_head', __CLASS__ . '::top_head', 2);		
		add_action('wp_head', __CLASS__ . '::bottom_head', 100);
		add_action('x_before_site_begin', __CLASS__ . '::x_before_site_begin');
		add_filter('the_content', __CLASS__ . '::the_content', 1000);
		add_filter('wp_nav_menu', __CLASS__ . '::wp_nav_menu');
		add_filter('acf/pre_save_post', __CLASS__ . '::my_acf_pre_save_post', 1, 2);
		add_shortcode('search_for_posts', __CLASS__ . '::print_search_results');
    	add_action('wp_head', __CLASS__ . '::count_postviews');

    	add_action('pre_post_update', __CLASS__ . '::approving_airports', 10, 2 );				
	    add_filter('redirect_post_location', __CLASS__ . '::filter_approved_post', 10, 2 );
	  
    /* hiding functional for "hide" button */
    add_action( 'admin_enqueue_scripts', __CLASS__ . '::admin_js_enqueue' );      
    add_action( 'wp_ajax_offblocks_hide_place', __CLASS__ . '::offblocks_hide_place_callback' );
    //add_action( 'wp_ajax_nopriv_offblocks_hide_place', __CLASS__ . '::offblocks_hide_place_callback' );
    add_filter('post_row_actions', __CLASS__ . '::offblocks_hide_post_actions', 10, 2 );
    add_filter('request', __CLASS__ . '::custom_filter_for_posts');
    add_filter('post_class', __CLASS__ . '::category_id_class');

		add_action('wp', __CLASS__ . '::my_init_',10,1);
	}


	public static function wp_nav_menu($menu)
	{
		return do_shortcode($menu);
	}


	public static function the_content($content)
	{
		return do_shortcode($content);
	}


	public static function init()
	{

	}


	public static function top_head()
	{
		if (!is_user_logged_in()) {
			print View::make('plugin/top_head.twig', [], ['wpautop' => false]);
		}

	}


	public static function bottom_head()
	{
		if (!is_user_logged_in()) {
			print View::make('plugin/bottom_head.twig', [], ['wpautop' => false]);
		}

	}


	public static function x_before_site_begin()
	{
		if (!is_user_logged_in()) {
			print View::make('plugin/x_before_site_begin.twig', [], ['wpautop' => false]);
		}
	}

public static function my_init_(){
	global $post, $user;
	$post_type = ((isset($post))?$post->post_type:'');
	//$user_id = get_current_user_id();
	if(isset($_GET['mode']) && $_GET['mode']=='editing' && !is_admin()){	
		$id = Plugin::createDuplicateOf($post->ID, $user->ID, $post_type);			
		//die("asaa".$post->ID);
		wp_safe_redirect(get_permalink($id)."&action=edit");
		//redirect to new edit page 
	}
}

//************************** APPROVING AIRPORTS FROM ADMIN-PANEL********************************************

public static function approving_airports( $post_ID, $data ) {
  global $post;
 $status = ((isset($post->ID))?get_post_status( $post->ID ):'');
 $parent =  ((isset($post->ID))?get_ancestors(  $post->ID, 'post' ):'');
 $cur_user_id = get_current_user_id();
 $offblockstime = current_time('Y-m-d H:i:s');

 $fields = ((isset($_POST['acf']))?$_POST['acf']:array());//get_fields($post->ID);
 $post_type = ((isset($post->ID))?get_post_type( $post->ID ):'');
 if( $status == 'draft' && $parent[0] && is_admin() && in_array($post_type, ['city','airport']) && current_user_can('administrator')) {  
	foreach($fields as $field => $value){ 
            update_field($field,$value,$parent[0]);
    }    
    update_field('editing', 0, $parent[0]);
    update_post_meta($parent[0], 'Last_edited_by', $post->post_author);
    update_post_meta($parent[0], 'Last_approved_by', $cur_user_id);
    update_post_meta($parent[0], 'Last_approved_date', $offblockstime);
    $deleted = wp_delete_post($post->ID);    
  }
  
}



/**********************************************USER REDIRECTING TO AIRPORTS LIST AFTER APPROVING ********************/

function filter_approved_post( $location, $post_id ) {
	global $post;
	print_r($post);
	$post_type = $post->post_type;//get_post_type( $post_id );
 	$status = $post->post_status;//get_post_status( $post_id );
 	$parent =  $post->post_parent;//get_ancestors(  $post_id, 'post' );
 	if( $status == 'draft' && $parent && is_admin() && in_array($post_type, ['city','airport']) && current_user_can('administrator'))  			
			return admin_url( "edit.php?post_type=$post_type" );
	return $location;
}





	public static function print_search_results(){
		global $post, $authordata;
		global $user;		
		$cur_user_id = get_current_user_id();
        
		$args = array(
			'author' => get_current_user_id(),
			'post_type' =>array('airport','city','place'),
			'post_status'=>'draft'
		);
        $posts = get_posts( $args );
        //print_r($posts);
        $i=0;
		$postsAr = array();
    $path = plugin_dir_url(__FILE__ );
    foreach ( $posts as $post ){
        	setup_postdata( $post );
        	$post_type = get_post_field( 'post_type');
        	$postsAr[$i]['gallery'] = get_field('gallery');
					$postsAr[$i]['type'] = get_post_type();
					if($postsAr[$i]['type'] == 'place' && !isset($postsAr[$i]['gallery'][0])){
        		$place_id = get_field('place_id');
						$place = Cities::get_one_google_place_cache($place_id);
            if(isset($place['photos'][0]) && $place['photos'][0] != ''){
						  $postsAr[$i]['gallery'][0]['sizes']['thumbnail']=$place['photos'][0];
            }
            else{
              $postsAr[$i]['gallery'][0]['sizes']['thumbnail'] = $path.'views/images/places_marker.png';
            }
					}
        	$postsAr[$i]['title'] = get_the_title();
        	$postsAr[$i]['permalink'] = get_permalink();
        	$postsAr[$i]['iata'] = get_field('iata');
        	$google_map = get_field('google_map');
			if(!isset($postsAr[$i]['gallery'][0])){
        		$meta = "https://maps.googleapis.com/maps/api/streetview/metadata?size=600x300&location=".$postsAr[$i]['title']."&key=".self::google_api_key__;
        		$url = "https://maps.googleapis.com/maps/api/streetview?size=600x300&location=".$postsAr[$i]['title']."&key=".self::google_api_key__;
        		$j = @file_get_contents($meta);
        		$metadata = json_decode($j, true);
			
 				if($metadata['status']=='OK'){      		
        			$postsAr[$i]['street_view_photo'] = Cities::get_furl($url);
        		}
        		else{
        			$postsAr[$i]['street_view_photo'] = $path.'views/images/'.(( $post_type=='airport')?'airport.png':'city.jpg');	
        		}
        		//echo "---".$postsAr[$i]['street_view_photo'];
        	}        	
        	$i++;
     }
        wp_reset_postdata();        
        $upload_dir = wp_upload_dir(); 
        //$upload_dir['baseurl'];        
      	return View::make('search.twig', ['header'=>"My Pending posts",
      									  'posts'=>$postsAr,      									  
      									  'upload_dir'=>$upload_dir], ['wpautop' => false]);
	}



	public static function get_local_time($lat, $lng){
		$request = self::timezone_rqst_str."?key=".self::timezonedb_key__."&format=json&by=position&lat=".$lat."&lng=".$lng;
		//echo $request;		
		$code = "obc_timezone_".md5($request);
		//echo $code;
		if ( false === ( $data = get_transient( $code ) ) ) {	
			$j = @file_get_contents($request);
			$data = json_decode($j,true);
			if(isset($data)){set_transient( $code, $data, DAY_IN_SECONDS );}			
		}
		$time = (isset($data['timestamp']))? date('H:i', $data['timestamp']):'';		
		$gmtoffset = (isset($data['gmtOffset']))? $data['gmtOffset']/3600:'';
		
		$result = array('time'=>$time, 
						'zoneName'=> (isset($data['zoneName']))? $data['zoneName']:'', 
						'timestamp' => (isset($data['timestamp']))? $data['timestamp']:'', 
						'abbreviation'=>(isset($data['abbreviation']))? $data['abbreviation']:'', 
						'gmtoffset'=>$gmtoffset);
		//print_r($data);
		//print_r($result);
		if($result['timestamp']==""){$result = self::get_local_time($lat, $lng);}		
		return $result;

	}




public static function my_acf_pre_save_post( $post_id, $form ) 
{
	global $post;
	$title = get_the_title();
	$id = get_the_ID();
	$fields = get_fields($id);
	$parent =  get_ancestors(  $post->ID, 'post' );
	$fields_original = get_fields($post->ID);
	$post_type = get_post_type( $post->ID );
	$status = get_post_status( $post->ID );
	//if we have changed fields, then create new post with status draft
	if(in_array($post_type,['airport', 'place', 'city'])) { 
		if($post_type == 'airport') { update_field('editing', 0); }		
  		if( isset($_POST['acf']) && $_POST['changed'] == "on" && !is_admin())
  		{     			
   			Plugin::my_project_updated_send_email($id);   			
  		} 
  		elseif($_POST['changed'] == "off")
  		{ 
  			$deleted = wp_delete_post($post->ID);
  		}
  	}
  	return $post->ID;  
}



/************************************SENDING EMAIL WITH INFO ABOUT CHANGES IN POSTS ***********************/
	
	

public static function my_project_updated_send_email( $post_id ) {
	global $post; 
	$fields_changed_obj = $google_map_field	= [];
	$parent = wp_get_post_parent_id( $post->ID );
    if(!is_admin()){
			/********************* PATTERN FOR CHANGED FIELD IS acf[field_key]-new_value_of_field *****/
    	$fields_changed = explode("%",$_POST['changed_fields_array']);    	
    	$j=0;
			for($i=0;$i<count($fields_changed);$i++){
				$field_val = explode("|",$fields_changed[$i]);												
				preg_match('/acf\[(field_\w{13})\]\[?\w{0,8}\]?\[?(field_\w{13})?\]?/', $field_val[0], $matches);				
				
				switch (count($matches)) {				   
    				case 2:
    					$temp_field = get_field_object($matches[1]);   
						/**** managing google maps field ****/
    					if($temp_field['type']=="google_map"){
    						if(!$google_map_field){
    							$j++;
    							$google_map_field[0]['field'] = $temp_field;
    							$google_map_field[0]['new_val'] .= $field_val[1].", ";    							
    						}
    						else{ $j++; $google_map_field[0]['new_val'] .= $field_val[1].(($j<3)?", ":""); }
    					}
    					/**** usual fields ****/
    					else{
        					$fields_changed_obj[$i]['field'] = get_field_object($matches[1]);
							$fields_changed_obj[$i]['new_val'] = $field_val[1];
						}
        				break;
        			case 3:
        			/*********************************IF THE FIELD IS REPEATER *********************************/
        				$fields_changed_obj[$i]['parent'] = get_field_object($matches[1]);
        				if( get_field($matches[1]) ){
							while( has_sub_field($matches[1]) ){        				
        						$fields_changed_obj[$i]['field'] = get_sub_field_object($matches[2]);
        						$fields_changed_obj[$i]['field']['value'] = get_sub_field($matches[2]);
        					}
        				}
						$fields_changed_obj[$i]['new_val'] = $field_val[1];
        				break;
				}
			}
		$fields_changed_obj = array_merge($fields_changed_obj,$google_map_field);//$field_val[1];
  		$post_title = get_the_title( $post_id );
  		$post_url = get_permalink( $post_id );
			$iata = get_field('iata');
  		$subject = 'Post has been updated';
			$current_user = wp_get_current_user();
			$message = "<div style='font-size:14px;'>";
  		$message .= "<p>Hello, admin! I would like to bring your attention on a new post published on the blog. </p>".
								 "<p>On the site ".get_bloginfo('name')." post ".$post_title.(isset($iata)? "(".$iata.")":"")." has been updated by 
								 <a href='".admin_url("user-edit.php?user_id=$current_user->ID&wp_http_referer=/wp-admin/users.php")."'>".$current_user->user_login.".</p>";
  		$message .= "<p>Details of the post follow:</p>";
		foreach($fields_changed_obj as $field){
			if(isset($field['field']['label'])){
				$message .= "<p><strong>".$field['field']['label'].
							((isset($field['parent']['label']))?" (".$field['parent']['label'].")":"").
							(($field['field']['type']=="google_map")?"( Address,Latitude,Longitude )":"").":</strong><br>";
				/**********************************************************************************************************************/
				/******************************* IF FIELD VALUE IS AN ARRAY (RELATED GOOGLE MAPS, CITIES, GALLERIES) ******************/
				/**********************************************************************************************************************/
				if(is_array($field['field']['value'])) {
					$value ='';
					$field['field']['value'] = Plugin::Capturing_updated_fields($field['field']['value']);					
				}				
				$message .= "Previous Value: ".((isset($field['field']['value']))?$field['field']['value']:"--- ").";<br>";

				/**********************************************************************************************************************/
				/************* IF NEW VALUE IS A STRING WITH DIFFERENT VALUES DIVIDED BY / (RELATED GOOGLE MAPS, CITIES, GALLERIES) ***/
				/**********************************************************************************************************************/
				if(substr_count($field['new_val'], '/')){					
					$new_fields_val = explode("/",$field['new_val']);			
					if($new_fields_val[count($new_fields_val)-1]==""){unset($new_fields_val[count($new_fields_val)-1]);}
					$field['new_val']  = Plugin::Capturing_updated_fields($new_fields_val);				
				}
				/**********************************************************************************************************************/
				$message .= "New Value: ".((isset($field['new_val']))?$field['new_val']:"--- ").";</p>";
			}
		}		
  		$message .="<p>You can approve the change <a href='".get_bloginfo('url')."/wp-admin/post.php?post=".$post_id."&action=edit'>here...</a> .</p>";  
  		$message .="Best regards, Web-system Offblocks.com <wordpress@offblocks.com></div>";
  		$headers = 'Content-type: text/html\r\n From: Web-system Offblocks.com <wordpress@offblocks.com>' . "\r\n";		
  		wp_mail( get_option('admin_email'), $subject, $message, $headers ); 
	}
}

	
/************************* FROM FIELDS ARRAY TO STRING ***********************/
	
function Capturing_updated_fields($fields_val){
		$count_fields = count($fields_val);	
		$value ='';$value_part ='';	$counter = 0;		
		foreach($fields_val as $value_part){		
				$counter++;
				if(get_the_title($value_part)){ 
						$value .= get_the_title($value_part).(($counter != $count_fields)?", ":"");//""((!next($new_fields_val))?"":",");
				}
				else{
						$value .= (($value_part !="")?$value_part:"-").(!($counter == $count_fields)?", ":"");
				}							
		}
		return $value;
}


/*************************SANITIZING DATA************************************/
	
	
public static function my_kses_post( $value ) {
	// is array
	if( is_array($value) ) {	
		return array_map(__CLASS__.'::my_kses_post', $value);	
	}	
	// return
	$value = wp_kses_post( $value );
	//echo $value; die();	
	return esc_html( $value );
}



// Post_id will be the original
public static function createDuplicateOf($post_id, $user_id, $type){
	global $wpdb;
	/*
	 * get the original post id
	 */
	$post = get_post( $post_id ); 
	//post status: editing
	$user_id = get_current_user_id();
	
    //update_field('editing', $user_id);
    $title = get_the_title();
	
	$id = get_the_ID();

	/*
	 * if post data exists, create the post duplicate
	 */
	if (isset( $post ) && $post != null) {
 
		/*
		 * new post data array
		 */		
    $args = array(
       'post_status'   => 'draft',
       'post_type'     => $type,
       'post_author'   => $user_id,//$user_ID,
       'post_title'    => $title,
       'ping_status'   => 'closed',//get_option('default_ping_status'),
       'post_parent'   => $id,
       'menu_order'    => 0,
       'to_ping'       => '',
       'pinged'        => '',
       'post_password' => '',
       'guid'          => '',
       'post_content_filtered' => '',
       'post_excerpt'  => '',
       'import_id'     => 0,
	);
		/*
		 * insert the post by wp_insert_post() function
		 */
		$new_post_id = wp_insert_post( $args );
 
		/*
		 * get all current post terms ad set them to the new post draft
		 */
		$taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}
 
		/*
		 * duplicate all post meta just in two SQL queries
		 */
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				if( $meta_key == '_wp_old_slug' ) continue;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}
update_field('editing', 0, $new_post_id);
return $new_post_id;
 
}
}
  
public static function count_postviews() {
  
/* ------------ Настройки -------------- */
$meta_key       = 'views';  // Ключ мета поля, куда будет записываться количество просмотров.
$who_count      = 0;            // Чьи посещения считать? 0 - Всех. 1 - Только гостей. 2 - Только зарегистрированных пользователей.
$exclude_bots   = 1;            // Исключить ботов, роботов, пауков и прочую нечесть :)? 0 - нет, пусть тоже считаются. 1 - да, исключить из подсчета.

global $user_ID, $post;
	if(is_singular()) {
		$id = (int)$post->ID;
		static $post_views = false;
		if($post_views) return true; // чтобы 1 раз за поток
		$post_views = (int)get_post_meta($id,$meta_key, true);
		$should_count = false;
		switch( (int)$who_count ) {
			case 0: $should_count = true;
				break;
			case 1:
				if( (int)$user_ID == 0 )
					$should_count = true;
				break;
			case 2:
				if( (int)$user_ID > 0 )
					$should_count = true;
				break;
		}
		if( (int)$exclude_bots==1 && $should_count ){
			$useragent = $_SERVER['HTTP_USER_AGENT'];
			$notbot = "Mozilla|Opera"; //Chrome|Safari|Firefox|Netscape - все равны Mozilla
			$bot = "Bot/|robot|Slurp/|yahoo"; //Яндекс иногда как Mozilla представляется
			if ( !preg_match("/$notbot/i", $useragent) || preg_match("!$bot!i", $useragent) )
				$should_count = false;
		}

		if($should_count)
			if( !update_post_meta($id, $meta_key, ($post_views+1)) ) add_post_meta($id, $meta_key, 1, true);
    //die('something1'); 
	}
	return true;
}
  
 



  public static function is_post_pending($type){
	    global $wpdb;
	    global $post;
	    $result = $wpdb->get_results("SELECT ID FROM $wpdb->posts WHERE post_status='draft' AND post_type = '$type' AND post_title = '$post->post_title'");
	    return $result;
	}
  


 // Find the distance between two places in Km
 public static function calculateDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
 {
  // convert from degrees to radians
  $latFrom = deg2rad($latitudeFrom);
  $lonFrom = deg2rad($longitudeFrom);
  $latTo = deg2rad($latitudeTo);
  $lonTo = deg2rad($longitudeTo);

  $lonDelta = $lonTo - $lonFrom;
  $a = pow(cos($latTo) * sin($lonDelta), 2) +
    pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
  $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

  $angle = atan2(sqrt($a), $b);
  // distance / 1000 = distance in km
  return round($angle*$earthRadius, 1);
 }



/* Hiding And Showing Functionality for the places*/
/* adding hide button */
  public static function offblocks_hide_post_actions( $actions, $post ) {
    if ( $post->post_type != 'place' ) {
        return $actions;
    }

    if(current_user_can('administrator')) {
      $is_hidden = get_metadata( "post", $post->ID, "hidden", true );
      $url = admin_url( 'edit.php?post_type=place');
      //echo $is_hidden;
      $place_class = (($is_hidden === "true")?"show_place":"hide_place");
      $place_button_text = (($is_hidden === "true")?"Show":"Hide");
      $actions['offblocks_place_hide'] = '<a class="'.$place_class.'" data-postid="'.$post->ID.'" href="#">'.$place_button_text.'</a>';
    }
      return $actions;
 }


/**adding js code */
public static function admin_js_enqueue(){

  $hideplace_css =".hidden_place{  	
  	background-image: -webkit-gradient( linear, left top, left bottom, from(rgba(91,35,110,0.4)), to(rgba(91,35,110,0.4)) ) !important;
  }";
  wp_register_style('custom_css_admin', false);
  wp_enqueue_style('custom_css_admin');  
  wp_add_inline_style('custom_css_admin',$hideplace_css);

  wp_register_script('offblocks_admin_functions', plugin_dir_url( __FILE__ ).'views/js/admin_functions.js' );    
  wp_enqueue_script('offblocks_admin_functions');
  wp_localize_script('offblocks_admin_functions', 'hideajax', 
		array(
			'url' => admin_url('admin-ajax.php'),
			'nonce' => wp_create_nonce('hideajax_nonce')
		)
	); 
}
/*hide/show callback*/

static function offblocks_hide_place_callback() {
  check_ajax_referer( 'hideajax_nonce', 'nonce_code' );
  //echo $_POST['act'];
	if($post_id = $_POST['post_id']){
		switch($_POST['act']){
			case "hide_place":echo update_metadata( 'post', $post_id, 'hidden', 'true');			
				break;
			case "show_place":echo update_metadata( 'post', $post_id, 'hidden', 'false');			
				break;
		}
	}
	wp_die(); 
}

/*Filter for hidden places*/

public static function custom_filter_for_posts($vars){   
    global $pagenow;
    global $post_type;
    
    $start_in_post_types=array('place'); 
 
    if( !empty($pagenow) && $pagenow=='edit.php' && in_array($post_type , $start_in_post_types) && !current_user_can('administrator')){                         
            $vars['meta_query']=array(
                       "relation"=>"AND",
                        array(
                        	'relation' => 'OR',
							array(
                           		"key"=>"hidden",                           
                           		"compare"=>"NOT EXISTS"                           		
                        	),
                        	array(
                           		'relation' => 'AND',
								array(
                           			"key"=>"hidden",                           
                           			"compare"=>"EXISTS"                           		
                        		),
                        		array(
                           			"key"=>"hidden",                           
                           			"value"=>"true",
                           			"compare"=>"!="                           		
                        		)
                        	)
                        )
            );             
    }    
    //print_r($vars);
    return $vars;
	}


public static function category_id_class($classes) {
		global $post;
		$hidden = get_metadata( "post", $post->ID, "hidden", true );
		if($post->post_type=="place" && $hidden == "true")
			$classes[] = "hidden_place";		
		return $classes;
	}	

}
