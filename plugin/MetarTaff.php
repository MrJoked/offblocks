<?php 

namespace OffBlocks;

class MetarTaff
{
  	const google_api_key__ 		 = "AIzaSyAF0N3AqZpy25gKyVPqayaEswt749qPJ4M";
  	const notams_api_key__ = "149c69a0-0c1d-11e8-b3bf-634332eec99c";
  	const notams_req_str__ = "https://v4p4sz5ijk.execute-api.us-east-1.amazonaws.com/anbdata/states/notams/notams-list";
	
	
	public static function Setup()
	{
		
    add_shortcode('metar', __CLASS__ . '::metar_shortcode');
		add_shortcode('taf', __CLASS__ . '::taf_shortcode');
    add_shortcode('qnh', __CLASS__ . '::beliefmedia_internoetics_metar_qnh');
    add_shortcode('temperature', __CLASS__ . '::beliefmedia_internoetics_metar_temp');
    add_shortcode('arfor', __CLASS__ . '::beliefmedia_internoetics_bom_arfor');

	}


public static function metar_shortcode ($atts, $content = null) {
  extract(shortcode_atts(array(
    'loc' => 'yssy',
    'tidy' => 1,
    'p' => 1,
    'cache' => 3600
  ), $atts));

    $transient = "beliefmedia_metar_$loc.$cache.metar";
    $dbresult =  get_transient($transient);

    if ($dbresult == true) {
      $result = $dbresult;

     } else {

      $result = self::beliefmedia_internoetics_noaa_metar($loc);
      set_transient($transient, $result, $cache);
    }

  if ($tidy) $result = str_replace('<br />', ' ', $result); 
  if ($p) $result = '<p>' . $result . '</p>';

 return ($result !="")?$result:"METAR is not available";
}



/*
	TAF Shortcode (http://shor.tt/z1)
	tidy attribute applies only to OZ metars
*/


public static function taf_shortcode ($atts, $content = null) {
  extract(shortcode_atts(array(
    'loc' => 'yssy',
    'tidy' => 1,
    'p' => 1,
    'cache' => 3600
  ), $atts));

    $transient = "beliefmedia_metar_$loc.$cache.taf";
    $dbresult =  get_transient($transient);

    if ($dbresult == true) {
      $result = $dbresult;

     } else {

      $result = self::beliefmedia_internoetics_noaa_taf($loc);
      set_transient($transient, $result, $cache);
    }

  if ($tidy) $result = str_replace('<br />', ' ', $result);
  if ($p) $result = '<p>' . $result . '</p>';

 return (!array_search($result, ["","<p></p>"]))?$result:"<p>TAF is not available</p>"; 
}






public static function beliefmedia_internoetics_noaa_metar($loc) {
  $loc = strtoupper($loc);
 
   if (substr($loc, 0, 1) == 'Y') {

     /* Create CURL Request */
     $postData = 'keyword=' . $loc . '&type=search&page=TAF';
     $ch = curl_init();
     curl_setopt($ch,CURLOPT_URL,'http://www.bom.gov.au/aviation/php/process.php');
     curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
     curl_setopt($ch,CURLOPT_HEADER, false); 
     curl_setopt($ch, CURLOPT_POST, count($postData));
     curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);   
     curl_setopt($ch,CURLOPT_USERAGENT,self::beliefmedia_internoetics_random_user_agent());
     $result = curl_exec($ch);
     curl_close($ch);
 
     preg_match_all('/<p class="product">(.*?)<\/p>/si', $result, $match);
     $result = $match[1][1];

   } else {

    $fileName = 'http://tgftp.nws.noaa.gov/data/observations/metar/stations/' . $loc . '.TXT';
    $metar = '';
    $fileData = @file($fileName) or $fileData="";//or die('METAR not available');
	
    if ($fileData != false && $fileData != "") {
     list($i, $date) = each($fileData);
      while (list($i, $line) = each($fileData)) {
       $metar .= ' ' . trim($line);
      }
     $result = trim(str_replace('  ', ' ', $metar));
    }
  }
 return (isset($result))?$result:"";
}


/*
	Get TAF - Queries either NOAA or BOM (AU)
	More? http://shor.tt/z1
*/


public static function beliefmedia_internoetics_noaa_taf($loc) {
  $loc = strtoupper($loc);
 
   if (substr($loc, 0, 1) == 'Y') {

     /* Create CURL Request */
     $postData = 'keyword=' . $loc . '&type=search&page=TAF';
     $ch = curl_init();
     curl_setopt($ch,CURLOPT_URL,'http://www.bom.gov.au/aviation/php/process.php');
     curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
     curl_setopt($ch,CURLOPT_HEADER, false); 
     curl_setopt($ch, CURLOPT_POST, count($postData));
     curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);   
     curl_setopt($ch,CURLOPT_USERAGENT,self::beliefmedia_internoetics_random_user_agent());
     $result = curl_exec($ch);
     curl_close($ch);
 
     preg_match_all('/<p class="product">(.*?)<\/p>/si', $result, $match);
     $result = $match[1][0];

   } else {

    $fileName = 'ftp://tgftp.nws.noaa.gov/data/forecasts/taf/stations/' . $loc . '.TXT';
    $taf = ''; $fileData = @file($fileName) or $fileData="";//or die('TAF not available');

    if ($fileData != false && $fileData != "") {
      list($i, $date) = each($fileData);
        while (list($i, $line) = each($fileData)) {
          $taf .= ' ' . trim($line);
        }
     $result = trim(str_replace('  ', ' ', $taf));
    }
  }

 return (isset($result))?$result:"";
}





/*
  Convert Naked Temp to Readable Integer
*/


public static function beliefmedia_internoetics_metar_temp_integer($temp) {
  $temp = str_replace('M', '-', "$temp");
 return intval($temp);
}


/*
  Get QNH from METAR Report
  Ref: ISA 1013.25, 29.92
  Since HPA is ICAO standard, defaults to true
  Naked PHP Usage on www.flight.org
*/


public static function beliefmedia_internoetics_metar_qnh($atts, $content = null) {
  extract(shortcode_atts(array(
    'loc' => 'yssy',
    'unit' => 1,
    'cache' => 3600
  ), $atts));

  $transient = "beliefmedia_metar_$loc.$cache.metar";
  $dbresult =  get_transient($transient);

  if ($dbresult == true  ) {
   $result = $dbresult;

    } else {
    $result = self::beliefmedia_internoetics_noaa_metar($loc);
    set_transient($transient, $result, $cache);
   }
   //echo "--".$result."--";
  if((!strpos($result,"not available")) && ($result!==""))
  {
    preg_match('/(A|Q)([0-9]{4})/', $result, $results);
    $altimeter_raw = $results['2'];

       if ( ($results[1] == 'A') && ($unit) ):
    $altimeter = round( ($altimeter_raw / 0.02953) / 100, 0);
      elseif ( ($results[1] == 'Q') && (!$unit) ):
    $altimeter = round(0.02953 * $altimeter_raw, 2);
      elseif ( ($results[1] == 'A') && (!$unit) ):
    $altdiv = ($altimeter_raw/100);
    $altimeter = number_format($altdiv , 2, '.', '');
      else:
    $altimeter = ltrim($altimeter_raw, '0');
      endif;
  }
 return (isset($altimeter))?$altimeter:"";
}


/*
  Get Temperature from METAR Report
  Since Celcius is ICAO standard, defaults to true
  Usage: beliefmedia_internoetics_metar_temp($report);
  Returned as array with all celcius and Fahrenheit values
  $tempArray = beliefmedia_internoetics_metar_temp($report);
  echo $tempArray['tempc']; echo $tempArray['dpc']; etc.

  Temperature (normalised integer °C) is 14 [temperature loc="kjfk" temp="1"] 
  Dew Point Temp (normalised integer °C) is 12 [temperature loc="kjfk" temp="2"] 
  Raw Temp (from Metar °C) is 14 [temperature loc="kjfk" temp="3"] 
  Raw Dew Point Temp (from Metar °C) is 12 [temperature loc="kjfk" temp="4"] 
  Temperature (°F) (normalised integer) is 57 [temperature loc="kjfk" temp="5"] 
  Dew Point Temp (°F) (normalised integer) is 54 [temperature loc="kjfk" temp="6"] 
  Raw report data is 14/12 [temperature loc="kjfk" temp="7"] 

*/


public static function beliefmedia_internoetics_metar_temp($atts, $content = null) {
  extract(shortcode_atts(array(
    'loc' => 'yssy',
    'temp' => 1,
    'int' => 1,
    'degrees' => 0,
    'cache' => 3600
  ), $atts));

  $transient = "beliefmedia_metar_$loc.$cache.metar";
  $dbresult =  get_transient($transient);

  if ($dbresult == true) {
   $result = $dbresult;
  } else {
   $result = self::beliefmedia_internoetics_noaa_metar($loc);
   set_transient($transient, $result, $cache);
  }

  preg_match('/(M?[0-9]{2})\/(M?[0-9]{2}|[X]{2})/', $result, $results);
  //echo "--".$results."--";
  if(isset($results[1]) && isset($results[2])) {
    $tempC = $results[1]; 
    $dpC = $results[2];
  
      /* Get temp C as Int */
    $tempC_int = self::beliefmedia_internoetics_metar_temp_integer($tempC); 
    $dpC_int = self::beliefmedia_internoetics_metar_temp_integer($dpC);

    /* Temp F */
    $tempF = ( round(1.8 * $tempC_int + 32) );
    $dpF = ( round(1.8 * $dpC_int + 32) );

   if ($temp == '1'):
     $temperature = $tempC_int;
   elseif($temp == '2'):
     $temperature = $dpC_int;
   elseif($temp == '3'):
     $temperature = $results[1];
   elseif($temp == '4'):
     $temperature = $results[2];
   elseif($temp == '5'):
     $temperature = $tempF;
   elseif($temp == '6'):
     $temperature = $dpF;
   elseif($temp == '7'):
     $temperature = $results[0];
   else:
     $temperature = ltrim($tempC, '0');
   endif;
  }

  if ($degrees) {
     if (strripos($temperature,'/') !== false) {
      $temperature = str_replace('/', '&deg;/', $temperature) . '&deg;';
     } else {
    $temperature = $temperature . '&deg;';
   }
  }
 return (isset($temperature))?$temperature:"";
}


/*
  Australian ARFOR Area Forecast from BOM
  http://www.internoetics.com/example-pages/arfor-wordpress-shortcode-example/
*/


public static function beliefmedia_internoetics_bom_arfor($atts, $content = null) {
  extract(shortcode_atts(array(
    'loc' => '21',
    'cache' => 3600
  ), $atts));

  $transient = "beliefmedia_metar_$loc.$cache.arfor";
  $dbresult =  get_transient($transient);

  if ($dbresult == true  ) {
   return $dbresult;

   } else {

   if (!ctype_digit($loc)) $loc = '21';

   /* Create CURL Request */
   $postData = 'keyword=' . $loc . '&type=search&page=TAF';
   $ch = curl_init();
   curl_setopt($ch,CURLOPT_URL,'http://www.bom.gov.au/aviation/php/process.php');
   curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
   curl_setopt($ch,CURLOPT_HEADER, false); 
   curl_setopt($ch, CURLOPT_POST, count($postData));
   curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);   
   curl_setopt($ch,CURLOPT_USERAGENT,self::beliefmedia_internoetics_random_user_agent());
   $result = curl_exec($ch);
   curl_close($ch);

   set_transient($transient, $result, $cache);
  return $result;
 }
}

public static function beliefmedia_internoetics_random_user_agent() {

    $userAgents=array(
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6",
        "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)",
        "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)",
        "Opera/9.20 (Windows NT 6.0; U; en)",
        "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; en) Opera 8.50",
        "Mozilla/4.0 (compatible; MSIE 6.0; MSIE 5.5; Windows NT 5.1) Opera 7.02 [en]",
        "Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; fr; rv:1.7) Gecko/20040624 Firefox/0.9",
        "Mozilla/5.0 (Macintosh; U; PPC Mac OS X; en) AppleWebKit/48 (like Gecko) Safari/48",
        "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36",
  "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 7.0; InfoPath.3; .NET CLR 3.1.40767; Trident/6.0; en-IN)",
  "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)",
  "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)",
  "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/5.0)",
  "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/4.0; InfoPath.2; SV1; .NET CLR 2.0.50727; WOW64)",
  "Mozilla/5.0 (compatible; MSIE 10.0; Macintosh; Intel Mac OS X 10_7_3; Trident/6.0)",
  "Mozilla/4.0 (Compatible; MSIE 8.0; Windows NT 5.2; Trident/6.0)",
  "Mozilla/4.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/5.0)",
  "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko",
  "Mozilla/1.22 (compatible; MSIE 10.0; Windows 3.1)"
    );
  $random = rand(0,count($userAgents)-1);
 return $userAgents[$random];
}
}