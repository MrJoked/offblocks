<?php

namespace OffBlocks;

use Twig_Environment;
use Twig_Extension_Debug;
use Twig_Loader_Filesystem;
use Twig_Loader_Chain;
use Twig_SimpleFunction;

class View
{

	static $twig;

	public static function Setup()
	{

		$debug = false;
		$cache = false;

		$viewFolder = plugin_dir_path(__FILE__) . 'views';
		$pathArray = [$viewFolder];

		$fileLoader = new Twig_Loader_Filesystem($pathArray);
		$loader = new Twig_Loader_Chain([$fileLoader]);

		$configArray = array(
			'autoescape' => false,
		);

		if ($debug) {
			$configArray['debug'] = true;
		}
		if ($cache) {
			$configArray['cache'] = $viewFolder . DIRECTORY_SEPARATOR . '_cache';
		}

		$twig = new Twig_Environment($loader, $configArray);

		if ($debug) {
			$twig->addExtension(new Twig_Extension_Debug());
		}

		$function = new Twig_SimpleFunction('action', function ($action) {
			return do_action(strtolower($action));
		});
		$twig->addFunction($function);

		self::$twig = $twig;

		add_shortcode('view', __CLASS__ . '::view');
		add_filter('view_before_render', __CLASS__ . '::view_before_render');

	}

	public static function view_before_render($vars)
	{
		return array_merge($vars, ['ajaxUrl' => admin_url('admin-ajax.php')]);
	}

	public static function view($attributes)
	{
		$a = (object)shortcode_atts([
			'view'    => '',
			'wpautop' => 'yes',
		], $attributes);

		if (!$a->view) {
			$a->view = $attributes[0];
		}

		$a->wpautop = ($a->wpautop == 'yes') ? true : false;

		return self::make($a->view . '.twig', ['attributes' => $attributes], ['wpautop' => $a->wpautop]);
	}

	public static function make($view, $vars = [], $options = [])
	{

		$options = (object)shortcode_atts(
			array(
				'shortcodes' => true,
				'wpautop'    => true,
			), $options
		);

		$vars = apply_filters('view_before_render', $vars);

		//render
		$html = self::$twig->render($view, $vars);

		// Parse shortcodes?
		if ($options->shortcodes) {
			$html = do_shortcode($html);
		}

		// Run through the horrible wpautop?
		if ($options->wpautop) {
			$html = wpautop($html);
		}

		return $html;
	}

}
