<?php

namespace OffBlocks;

use OFfBlocksLoad\Plugin as Load;


class Airports
{
	const google_api_key__ 		 = "AIzaSyAF0N3AqZpy25gKyVPqayaEswt749qPJ4M";
	const notams_api_key__ = "149c69a0-0c1d-11e8-b3bf-634332eec99c";
  const notams_req_str__ = "https://v4p4sz5ijk.execute-api.us-east-1.amazonaws.com/anbdata/states/notams/notams-list";
	
	
	public static function Setup()
	{
		ob_start();
		add_shortcode('airport_info', __CLASS__ . '::print_info');
		add_shortcode('airport_general', __CLASS__ . '::print_general');
		add_shortcode('airport_header', __CLASS__ . '::print_header');
		add_shortcode('airport_arrival', __CLASS__ . '::print_arrival');
		add_shortcode('airport_ground_ops', __CLASS__ . '::print_ground_ops');
		add_shortcode('airport_departure', __CLASS__ . '::print_departure');
		add_shortcode('airport_wx_notams', __CLASS__ . '::print_wx_notams');
		add_shortcode('airport_gallery', __CLASS__ . '::print_gallery');
		add_shortcode('airport_cities', __CLASS__ . '::print_cities');
		add_shortcode('the_end_edit', __CLASS__ . '::print_the_end');

		//add_filter('acf/pre_save_post', __CLASS__ . '::my_acf_pre_save_post', 1, 2);
		
		add_action('shutdown', __CLASS__ . '::shutdown_php' );    

		add_action( 'wp_enqueue_scripts', __CLASS__ . '::airports_enqueque_scripts' );
		
		add_action('wp_ajax_nopriv_metartaf', __CLASS__ . '::metartaf_callback');
		add_action('wp_ajax_metartaf', __CLASS__ . '::metartaf_callback');


		
		//wp_enqueue_script( 'google-map', get_stylesheet_directory_uri() . '/assets/js/google-map.js', array(), '1.0.0', true );
		//wp_register_script( 'google-scripts','https://maps.googleapis.com/maps/api/js?key=AIzaSyAF0N3AqZpy25gKyVPqayaEswt749qPJ4M');

	}
	public static function airports_enqueque_scripts($val){
		global $post;
		if(isset($post)){$post_type = get_post_type( $post->ID );}
		wp_enqueue_script( 'google-map', get_stylesheet_directory_uri() . '/assets/js/google-map.js', array(), '1.0.0', true );
		wp_register_script( 'google-scripts','https://maps.googleapis.com/maps/api/js?key=AIzaSyAF0N3AqZpy25gKyVPqayaEswt749qPJ4M');
		wp_enqueue_script( 'moment', plugin_dir_url( __FILE__ )."views/js/moment.js");//'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js' );//plugin_dir_url( __FILE__ )."views/js/moment.js" );
		wp_enqueue_script( 'momentjs', plugin_dir_url( __FILE__ )."views/js/momenttz.js");//'https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.4.1/moment-timezone-with-data-2010-2020.min.js');//
		wp_enqueue_script( 'suncalc', plugin_dir_url( __FILE__ )."views/js/suncalc.js");//'https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.4.1/moment-timezone-with-data-2010-2020.min.js');//
		if(isset($post_type) && ($post_type == 'airport')){
			wp_enqueue_script( 'offblocks-tabs-history', plugin_dir_url( __FILE__ ).'views/js/tabs_history.js' );
		}
	}

	public static function print_header()
	{
		global $post;
		$fields = get_fields($post->ID);
		$country = get_field('country');
		$google_map = get_field('google_map');
		$field_time = get_field('local_time');
   		
		$timeArray = Plugin::get_local_time($google_map['lat'],$google_map['lng']);//date('H:i', time()+$tmm*$onehour) . "<br>\n";
   		$time = $timeArray['time'];  	
   		$timestamp = (isset($timeArray['timestamp']))? $timeArray['timestamp']:0;
   		$gmtoffset = (isset($timeArray['gmtoffset']))? $timeArray['gmtoffset']:'';
   		$sunrise = date_sunrise($timestamp, SUNFUNCS_RET_STRING, $google_map['lat'], $google_map['lng'], 90.583333, $gmtoffset);
		  $sunset =  date_sunset($timestamp, SUNFUNCS_RET_STRING, $google_map['lat'], $google_map['lng'], 90.583333, $gmtoffset);		
		$edit="hide";
		$submit = "";
		$disabled = "";
		if(is_user_logged_in()){
			$id = get_current_user_id();
			$type_reg=get_user_meta($id,'type_reg',true);
			// if no user editing post
			$editing = self::is_airport_pending();			
			//die();
			$disabled ='';
			if(isset($editing[0]->ID)){$disabled='disabled';}
			// if user is pilot
			if( current_user_can('edit_airports') ){
				$edit="show";
			}
			//if($type_reg=="pilot"){$edit="show";}
			// if edit mode then swithch to save button 
			if(isset($_GET['action']) && $_GET['action']=='edit'){$submit='save';}
			else{$submit='edit';}			
		}
		/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
		if(!$post->post_parent){$reference = get_permalink();}
		else{$reference = get_permalink($post->post_parent);}
		//wp_reset_postdata();
		/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/		
		$path_to_plugin = plugin_dir_url( __FILE__ );
		print View::make('airports/airport_header.twig',   ['airport'=>$fields,
															'path'=>$path_to_plugin,
															'country'=>$country, 
															'sunrise'=>$sunrise, 
															'location'=>$google_map,
															'timeZone' =>$timeArray['zoneName'],
															'abbreviation' =>$timeArray['abbreviation'],
														  	'sunset'=>$sunset,
															'localtime'=>$time, 
															'edit'=>$edit, 
															'disabled'=>$disabled, 
															'submit'=>$submit, 
															'ref' => $reference], ['wpautop' => false]);
	}
	
	public static function print_info()
	{
		global $post;

		$fields = get_fields($post->ID);
		$runway = get_field('runways');
		
		// DENSITY ALTITUDE CALCULATIONS
		$icao = get_field('icao');	
		$qnh = do_shortcode('[qnh loc="'.$icao.'"]');
		$tmpr = do_shortcode('[temperature loc='.$icao.' temp="1"]');	
		$elevation = get_field('elevation_f');	
		$dens_alt = ((1013-$qnh)*28+$elevation)+120*($tmpr-(15-((1013-$qnh)*28+$elevation)/500));
    $path_to_plugin = plugin_dir_url( __FILE__ );
		return View::make('airports/airport_overview.twig', ['airport'=>$fields,
																												 'dens_alt'=>$dens_alt,																												
																												 'runway'=>$runway,
                                                         'path'  =>$path_to_plugin], ['wpautop' => false]);
	}

	public static function print_general()
	{
		global $post;
		$fields = get_fields($post->ID);
    $path_to_plugin = plugin_dir_url( __FILE__ );
		return View::make('airports/airport_general.twig', ['airport'=>$fields,
                                                        'path'  =>$path_to_plugin], ['wpautop' => false]);
	}

	public static function print_arrival()
	{
		global $post;
		$fields = get_fields($post->ID);
    $path_to_plugin = plugin_dir_url( __FILE__ );
		return View::make('airports/airport_arrival.twig', ['airport'=>$fields,
                                                        'path'  =>$path_to_plugin], ['wpautop' => false]);
		
	}

	public static function print_ground_ops()
	{
		global $post;
		$fields = get_fields($post->ID);
    $path_to_plugin = plugin_dir_url( __FILE__ );
		return View::make('airports/airport_ground_ops.twig', ['airport'=>$fields,
                                                           'path'  =>$path_to_plugin], ['wpautop' => false]);
	}

	public static function print_departure()
	{
		global $post;
		$fields = get_fields($post->ID);
    $path_to_plugin = plugin_dir_url( __FILE__ );
		return View::make('airports/airport_departure.twig', ['airport'=>$fields,
                                                          'path'  =>$path_to_plugin], ['wpautop' => false]);
	}

	/***********************************PRINT NOTAMS AND WX INFO************************/
	
	
	public static function print_wx_notams()
	{
		global $post;
		

		$fields = get_fields($post->ID);
		$icao = get_field('icao');		
		$google_map = get_field('google_map');
		$wx_general_info = get_field('wx_general_info');

		$weather_shortcode = do_shortcode('
			[wunderground location="'.$icao.'" class="wunder-current-cond" layout="current" hidedata="search" measurement="metric"]
			[wunderground location="'.$icao.'" class="wunder-forecast" numdays="3" layout="table-horizontal" measurement="metric" showdata="highlow,alert,daynames,pop,icon,text,conditions,date" ]');		
		
		$metartaf = do_shortcode('<p>SA [metar p="0" loc="'.$icao.'"]</p> [taf loc="'.$icao.'"]');				
		$metartaf = str_replace('TAF TAF', 'TAF', $metartaf);				
		
		$notams = self::get_notams($icao);
    $path_to_plugin = plugin_dir_url( __FILE__ );
		return View::make('airports/airport_wx-notams.twig', ['weather_shortcode'=>$weather_shortcode, 
																													'metartaf'=>$metartaf, 
																													'notams'=>$notams, 
																													'wx_general_info'=>$wx_general_info,
                                                          'path'  =>$path_to_plugin], 
											['wpautop' => false]);
	}

	
/***********************************GETTING NOTAMS FROM SERVICE************************/
	
	
public static function get_notams($icao)
{
    global $post;
		$notams_all = [];
		$data = [];
    $request = self::notams_req_str__."?format=json&type=airport&locations=".$icao."&api_key=".self::notams_api_key__;
	  $code = "obc_notams_".md5($request);
		if ( false === ( $data = get_transient( $code ) ) ) {	
		  	$j = @file_get_contents($request);
			  $data = json_decode($j,true);
			  set_transient( $code, $data, 3*HOUR_IN_SECONDS );		
		}
		if($data != ""){
    		for($i=0; $i<count($data);$i++){
						$pattern = '/(A[0-9]{4}\/)([0-9]{2})/';//'/^A([0-9]{4,4})\/([0-9]{2,2})/';
						$replacement = '<strong>\1\2</strong>';
						$temp = preg_replace($pattern, $replacement, $data[$i]['all']);
      			$notams_all[] = str_replace("\n", "</br>", $temp); 			
    		}
		}
    return $notams_all;
}
	
	
	public static function print_gallery()
	{
		global $post;
		$fields = get_fields($post->ID);
    $path_to_plugin = plugin_dir_url( __FILE__ );
		return View::make('airports/airport_gallery.twig', ['airport'=>$fields,
                                                        'path'  =>$path_to_plugin], ['wpautop' => false]);
	}
	
	
	

	public static function print_cities()
	{
		global $post;
		$country = get_field('country',$post->ID );
		$cities = get_field('cities',$post->ID );
		$google_map = get_field('google_map',$post->ID);
		$i=0; $gallery = array(''); 
		$cities_coord = array(''); 
		foreach ($cities as $post) {
			setup_postdata($post);
			$images = get_field( "gallery" );
			if(!isset($images[0])){
        		$name = str_replace(' ','%20',get_the_title());
        		$meta = "https://maps.googleapis.com/maps/api/streetview/metadata?size=600x300&location=".$name."&key=".self::google_api_key__;
        		$url = "https://maps.googleapis.com/maps/api/streetview?size=600x300&location=".$name."&key=".self::google_api_key__;
        		$j = @file_get_contents($meta);
        		$metadata = json_decode($j, true);
 				 										
 				if($metadata['status']=='OK'){
 					$url = Cities::get_furl($url);
        			$images[0]['street_view_photo'] = $url;        			
      			}	 
      			else{
      				$path = plugin_dir_url( __FILE__ );        			
      				$images[0]['street_view_photo'] = $path.'views/images/city.jpg';        			
      			}
      		}
			$titles[$i] = get_the_title();
			$guids[$i] = get_permalink(); 
			$gallery[$i] = $images[0];
			$cities_coord[$i] = get_field('google_map');
			$i++; 			
		}
		//$cities = get_field('cities',$post->ID);
		$upload_dir = wp_upload_dir(); 
    $path_to_plugin = plugin_dir_url( __FILE__ );
		return View::make('airports/airport_cities.twig', ['cities'=>$cities,
														   'cities_coord' =>$cities_coord, 
														   'titles'=>$titles,
														   'guids'=> $guids, 
														   'gallery' => $gallery,
														   'google_map' => $google_map,
														   'upload_dir' => $upload_dir['baseurl'], 
														   'country' => $country,
                               'path'  =>$path_to_plugin], ['wpautop' => false]);
	}

	
	
	public static function print_the_end( $atts )
	{		
		$path_to_plugin = plugin_dir_url( __FILE__ );
		if(isset($atts['mode']) && $atts['mode']=='noedit'){$mode = 0;}
		else{$mode = 1;}
		return View::make('airports/airport_the_end.twig', ['form'=>$mode,'path'=>$path_to_plugin], ['wpautop' => false]);
	}
	


/**********************************************UPDATING FIELD EDITING OF POST ****************************************/

public static function shutdown_php(){
	global $post;
	if(isset($_GET['action']) && $_GET['action']=='edit' && !is_admin()){
		$user_id = get_current_user_id(); 
    	update_field('editing', $user_id, $post->ID);   
    }  
}




public static function is_airport_pending(){
	global $wpdb;
	global $post;
	$result = $wpdb->get_results("SELECT ID FROM $wpdb->posts WHERE post_status='draft' AND post_type = 'airport' AND post_title = '$post->post_title'");
	return $result;
	}

}
	
	
	
	
	
	