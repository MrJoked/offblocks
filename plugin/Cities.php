<?php

namespace OffBlocks;

use OFfBlocksLoad\Plugin as Load;
use libchart\Plugin as PChart;

class Cities
{
	const google_api_key__ 		 = "AIzaSyAF0N3AqZpy25gKyVPqayaEswt749qPJ4M";
	const places_request__ 		 = "https://maps.googleapis.com/maps/api/place/textsearch/json";
	const places_photo_request__ = "https://maps.googleapis.com/maps/api/place/photo";
	static $addressComponentsMap = [
		'country'                     => 'country',
		'administrative_area_level_1' => 'state',
		'locality'                    => 'city',
	];
	
	public static function Setup()
	{		
		add_shortcode('city_header', __CLASS__ . '::print_city_header');
		add_shortcode('city_quick_facts', __CLASS__ . '::print_quick_facts');
		add_filter( 'wunderground_cache_time', __CLASS__ . '::change_wu_cahe_time' );
		add_shortcode('city_places', __CLASS__ . '::print_places');

		add_shortcode('city_stay_safe', __CLASS__ . '::print_stay_safe');	
		add_shortcode('city_airports', __CLASS__ . '::print_city_airports');
		add_shortcode('place_edit', __CLASS__ . '::place_form');
		add_shortcode('city_end_edit', __CLASS__ . '::print_city_end');
		
		add_action( 'wp_ajax_offblock_it', __CLASS__ . '::offblock_it_callback');
		add_action( 'wp_ajax_nopriv_offblock_it', __CLASS__ . '::offblock_it_callback'); 
		
		add_action( 'wp_ajax_show_approved_place', __CLASS__ . '::show_approved_place_callback');
		add_action( 'wp_ajax_nopriv_show_approved_place', __CLASS__ . '::show_approved_place_callback'); 
		
		add_action( 'wp_ajax_get_results', __CLASS__ . '::get_results_callback');
		add_action( 'wp_ajax_nopriv_get_results', __CLASS__ . '::get_results_callback');
		
		add_action( 'wp_ajax_edit_place', __CLASS__ . '::edit_place_callback');
		add_action( 'wp_ajax_add_place', __CLASS__ . '::add_place_callback');

		add_action( 'wp_enqueue_scripts', __CLASS__ . '::cities_enqueque_scripts' );
		add_action( 'wp_enqueue_scripts', __CLASS__ . '::myajax_data', 99 );

		
		add_action('acf/input/admin_footer', __CLASS__.'::my_acf_input_admin_footer');

		require_once("libchart/libchart/classes/libchart.php");	
		//require_once( ABSPATH . "wp-admin" . '/includes/file.php');
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		require_once( ABSPATH . 'wp-admin/includes/media.php' );
		
	}


	
	public static function cities_enqueque_scripts($val){
		global $post;
		if(isset($post)){$post_type = get_post_type( $post->ID );}
		if(isset($post_type) && ($post_type == 'city' || $post_type == 'place')){
			wp_enqueue_script( 'google-map', get_stylesheet_directory_uri() . '/assets/js/google-map.js', array(), '1.0.0', true );
			wp_enqueue_script( 'offblocks-functions', plugin_dir_url( __FILE__ ).'views/js/functions.js' );
			wp_enqueue_script( 'offblocks-tabs-history', plugin_dir_url( __FILE__ ).'views/js/tabs_history.js' );
			wp_enqueue_script( 'google-places_lib','https://maps.googleapis.com/maps/api/js?key='.self::google_api_key__.'&language=en&libraries=places');		
			wp_enqueue_style('places_style', plugin_dir_url( __FILE__ )."views/css/places.css");
		}
	}


	
	public static function myajax_data(){

	wp_localize_script('google-map', 'myajax', 
		array(
			'url' => admin_url('admin-ajax.php'),
			'comment_url' => site_url('/wp-comments-post.php')
		)
	);  

	}


	public static function print_city_header()
	{
		global $post;
		$fields = get_fields($post->ID);
		$country = get_field('country');
		$google_map = get_field('google_map');
		$field_time = get_field('local_time');
		$title = get_the_title();
		$ref = get_permalink();

   		$timeArray = Plugin::get_local_time($google_map['lat'],$google_map['lng']);//date('H:i', time()+$tmm*$onehour) . "<br>\n";
   		$time = $timeArray['time'];
   		$timestamp = (isset($timeArray['timestamp']))? $timeArray['timestamp']:0;
   		$gmtoffset = (isset($timeArray['gmtoffset']))? $timeArray['gmtoffset']:'';
   		//get_local_time()
		//echo $time;
		$sunrise = date_sunrise($timestamp, SUNFUNCS_RET_STRING, $google_map['lat'], $google_map['lng'], 90.583333, $gmtoffset);
		$sunset = date_sunset($timestamp, SUNFUNCS_RET_STRING, $google_map['lat'], $google_map['lng'], 90.583333, $gmtoffset);
		$edit="hide";
		$submit = "";
    $editing = Plugin::is_post_pending('city');			
		$disabled = (isset($editing[0]->ID))?'disabled':'';
    //echo $disabled;
    
		if(is_user_logged_in()){
			$id = get_current_user_id();
			$type_reg=get_user_meta($id,'type_reg',true);
			// if no user editing post
			$editing = get_field('editing');
			// if user is pilot
			if( current_user_can('edit_cities') ){
				$edit="show";
			}
			// if edit mode then swithch to save button 
			if(isset($_GET['action']) && $_GET['action']=='edit'){$submit='save';}
			else{$submit='edit';}			
			$logged_in_all = 1;
		}
		else{
			$logged_in_all = 0;
		}
		$path_to_plugin = plugin_dir_url( __FILE__ );
		/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
		if(!$post->post_parent){$reference = get_permalink();}
		else{$reference = get_permalink($post->post_parent);}
		//wp_reset_postdata();
		/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/	
		if(isset($_GET['place_updated']) && $_GET['place_updated'] ==1)	{$place_upd = 1;}
		else {$place_upd = 0;}
		
		$ajax_get_results = wp_create_nonce("ajax_get_results");		
		print View::make('cities/city_header.twig', ['city'=>$fields,
													 'city_ref'=>$ref,
													 'title'=>$title,
													 'country'=>$country, 
													 'sunrise'=>$sunrise, 
													 'sunset'=>$sunset,
													 'localtime'=>$time, 
													 'edit'=>$edit, 
													 'path'=>$path_to_plugin,
													 'ajax_get_results'=>$ajax_get_results,													 
													 'post_id' =>$post->ID,
													 'disabled'=>$disabled, 
													 'submit'=>$submit,
													 'logged_in_all'=>$logged_in_all,
													 'place_upd' => $place_upd,
													 'ref' => $reference], 
													 ['wpautop' => false]);
	}




	
	public static function print_quick_facts()
	{
		global $post;

		$fields = get_fields($post->ID);
		$title = get_the_title();
		$country = get_field("country");
		$google_map = get_field("google_map");
		$airports = get_field("airports");
		$icao = get_field('icao',$airports[0]->ID);

		//***************************************TIME***********************************
		$field_time = get_field('local_time');
		$title = get_the_title();
   		
   		$timeArray = Plugin::get_local_time($google_map['lat'],$google_map['lng']);//date('H:i', time()+$tmm*$onehour) . "<br>\n";
   		$time = $timeArray['time'];

   		$dayofweek = strftime("%A", strtotime($time));
   		/********************WUNDERGROUND DIAGRAM**************************/   			
		$chart_img = Cities::wunderground_hourly_forecast('http://api.wunderground.com/api/9379a0d44c885f9b/hourly/q/'.$icao.'.json', 'temperature');
		//echo $google_map['address'];
		$weather_current = do_shortcode('
			[wunderground location="'.$icao.'" class="65565" layout="current-for-City" hidedata="search" measurement="metric"]
			<br>'.$chart_img.'
			[wunderground location="'.$icao.'" class="3455666" numdays="7" layout="table-horizontal-for-City" measurement="metric" showdata="highlow,daynames,icon" ]</p>');		
		
		$currencies = get_field("currencies",$country->ID);
		//$plugs = get_field("plugs",$country->ID);
		$upload_dir = (object) wp_upload_dir();
		$languages = get_field("languages");
	  $path_to_plugin = plugin_dir_url( __FILE__ );
		return View::make('cities/city_quick_facts.twig', 
			['city'=>$fields,
			 'weather_current'=>$weather_current, 
			 'currencies'=>$currencies, 
			 'day_of_week'=>$dayofweek, 
			 'title'=>$title, 
			 'time'=>$time, 
			 'languages'=> $languages,
			 'country' => $country,
			 'upload_dir' =>$upload_dir->url,		
       'path'       =>$path_to_plugin
			 ], 
			 ['wpautop' => false]);
	}

public static function change_wu_cahe_time($num){	
	return 3600*6;
}




/*****************************************PRINT PLACES***************************************/

public static function print_places($atts){
	global $post;
		
	if(!isset($post)){
		if(isset($atts['city_id'])){			
			$post = get_post($atts['city_id']);
			setup_postdata($post);
		}
	}

	$ref = get_permalink();
	$fields = get_fields($post->ID);	
	$google_map = get_field('google_map');
	$location = $google_map['lat'].",".$google_map['lng'];
	
	/***********OVERRIDE LOCATION AND RADIUS PARAMETERS IF USER IS IN THE CITY ***********************/
	
	if(abs($google_map['lat']-$atts['user_lat'])<1 && abs($google_map['lng']-$atts['user_lng'])<1){          
        $location = $atts['user_lat'].",".$atts['user_lng'];
        $atts["radius"] = '2000';
     }		
	

				/********************************MULTIPLE TYPE REQUEST********************************/
	$types = explode(", ", $atts["type"]);		
	$tmp_arr = array();
	for($i=0;$i<count($types);$i++){
		$places_info[$i] = self::get_google_place_cache($location, $atts["radius"], $types[$i]);
		for($j=0;$j<count($tmp_arr); $j++){
			for($jp=0;$jp<count($places_info[$i]); $jp++){
			//if distanse between place and city center is more than radius or in case of duplicating place id(approved and from google) unset place
				$place_location = $places_info[$i][$jp]['location'];
				$real_distance = Plugin::calculateDistance($place_location['lat'],$place_location['lng'],$google_map['lat'], $google_map['lng']);
				
				if($real_distance > $atts["radius"]){ 
					//echo $places_info[$i][$jp]['name']." - ".$real_distance." - ".$atts["radius"]."; ";
					unset($places_info[$i][$jp]); 
				}				
				if(	isset($tmp_arr[$j]["place_id"]) && 
					isset($places_info[$i][$jp]["place_id"]) && 
					($tmp_arr[$j]["place_id"] == $places_info[$i][$jp]["place_id"]))				   
				  { unset($places_info[$i][$jp]); }
			}
		}
		$tmp_arr = array_merge($tmp_arr, $places_info[$i]);
	}
	$places_info = $tmp_arr;
				/********************************MULTIPLE TYPE REQUEST********************************/

	$places_id_array = array();
	for($i = 0; $i < count($places_info); $i++){
		$places_info[$i]['weekday_text'] = self::get_opening_hours_from_cache($places_info[$i]['place_id']);
		$ref = $places_info[$i]['photos'][0]['photo_reference'];		
		$request = self::places_photo_request__."?maxheight=200&photoreference=".$ref."&key=".self::google_api_key__;
		if($ref != ''){	
			$places_info[$i]['photos'][0]['url'] = self::get_furl($request);
			}
		else{$places_info[$i]['photos'][0]['url'] = '';}
		if($is_pending = self::is_post_pending($places_info[$i]['place_id'])){$places_info[$i]['is_pending'] = $is_pending;}
		$places_id_array[$i] = $places_info[$i]['place_id'];
	}		

	
				/******************************SHOW APPROVED PLACES***********************************/
	$tmp_arr = array();
	$approved_places = array();
  $city_id = $post->ID;
	for($i=0;$i<count($types);$i++){
    $types[$i] = str_replace('+', '_', $types[$i]);
		$approved_places = self::get_approved_places($places_id_array, $types[$i],$city_id);	
    //print_r($approved_places);
			if(isset($approved_places)){$tmp_arr = array_merge($tmp_arr, $approved_places);}		
	}

	$approved_places = $tmp_arr;
	
	if(isset($approved_places)){ 
		$count = count($approved_places);
		for($i = 0; $i < $count; $i++){
			//creating array with approved places id from google to hide the same places from google response
			$approved_places_id[$i] = $approved_places[$i]['place_id'];
			/*!!!!!!!!*/						
		}
		$places_info = array_merge($approved_places, $places_info);
	}
	if(!isset($approved_places_id)){$approved_places_id = array();}

				/**************************CREATING NONCE-CODE FOR 3 TYPES OF AJAX-REQUESTS*********************/
	$ajax_nonce = wp_create_nonce("google_request_nonce");
	$ajax_nonce_app = wp_create_nonce("ajax_nonce_app");
	//$ajax_get_results = wp_create_nonce("ajax_get_results");

	if(is_user_logged_in()){$logged_in = 1;}
	else{$logged_in = 0;}
	//print_r($atts); die();
	$path_to_plugin = plugin_dir_url( __FILE__ );
  	$path_to_admin = admin_url();  
	return View::make('cities/city_eating_out.twig', ['places'=>	$places_info,
													  'approved_places_id'		=>	$approved_places_id, 
													  'location'				=>	$google_map,
													  'ajax_nonce'				=>	$ajax_nonce, 
													  'type'					=>	$atts["type"],
													  'ajax_nonce_app'			=>	$ajax_nonce_app,
														'city_id' 				=> 	$atts['city_id'],
													  'header' 					=>	$atts['header'],
													  'logged_in'				=>	$logged_in,														
														'path'					=> 	$path_to_plugin,
                            							'adminpath'				=> 	$path_to_admin
														],														
													  ['wpautop' => false]);	
														
}





//**********************************GET APPROVED PLACES*****************************************************/

public static function get_approved_places($places_id, $type, $city_id){
	global $post;
	global $wpdb;	
	// присваивает <body text='black'>
	//$type = str_replace(",", "', '", $type);
	$type_id = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_type = 'place_sub_category' AND post_title = '" . $type . "';" );	
	$arCity = serialize( array($city,) );	
	$approved_places_posts = get_posts(array(
		'numberposts'	=> 0,		
		'post_type'		=> 'place',
		'post_status' 	=> 'publish',
		'meta_key' => 'rating',
   		'orderby' => 'place rating',
    	'order' => 'DESC',
		'meta_query'	=> array(
			'relation'		=> 'AND',
			array(
				'key'	 	=> 'city',
				'value'	  	=> $city_id,
				'compare' 	=> '=',
			),
			array(
				'key'	  	=> 'sub_category',
				'value'	  	=> $type_id,
				'compare' 	=> '=',
			),			
		),
	));	
	$i = 0;
	foreach( $approved_places_posts as $post ) {
		setup_postdata( $post );
		$approved_places[$i]['name'] = get_the_title();		
		if(have_rows('opening_times')){
			while (have_rows('opening_times')) {
			 the_row();
			 if(isset($approved_places[$i]['opening_hours'])){
				 	$approved_places[$i]['opening_hours'] .= get_sub_field('week_day').": ".get_sub_field('opening_time')."</br>";			 
				 }
			 else{
			 		$approved_places[$i]['opening_hours'] = get_sub_field('week_day').": ".get_sub_field('opening_time')."</br>";			 
				 }
			}
		}
		$sub_category = get_field('sub_category');		
		$approved_places[$i]['types'] =  get_the_title($sub_category); 
		$approved_places[$i]['hidden'] = get_metadata( 'post', get_the_ID(), 'hidden', 'true' );
		$approved_places[$i]['rating'] = get_field('rating');
		/******************GET THE FIRST PLACE PHOTO********************/
		$place_id = get_field('place_id');
		$gallery = get_field('gallery');
		/*THIS ARRAY IS CREATED FOR GALLERY WHERE IS NO PICTURES*/
		$gallery_with_no_pic = get_field('gallery', false, false);

		if( isset($gallery[0]) ){
			$approved_places[$i]['photos'][0]['urlap'] = $gallery[0]['sizes']['thumbnail'];
    	}
 	    else{  	$place = self::get_one_google_place_cache($place_id);
						$approved_places[$i]['photos'][0]['urlap'] = $place['photos'][0];        
				}
		
		/**************************************************************/
		$approved_places[$i]['place_id'] = $place_id;

		$approved_places[$i]['website'] = get_field('website');
		$approved_places[$i]['phone_number'] = get_field('phone_number');

		$approved_places[$i]['weekday_text'] = self::get_opening_hours_from_cache(get_field('place_id'));
		$overview = get_field('overview');
		$overview = self::tokenTruncate($overview, 200); 
		
		$approved_places[$i]['review'] = $overview;
		$approved_places[$i]['approved'] = 1;
		$i++;
    wp_reset_postdata(); 
	}
	if(isset($approved_places)){
		return $approved_places;	
	}
	return array();
}





/********************SEPARATE DEFAULT PART OF OVERVIEW (IF OVERVIEW IS VERY LONG)***********/

public static function tokenTruncate($string, $your_desired_width) {
  $parts = preg_split('/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE);
  $parts_count = count($parts);

  $length = 0;
  $last_part = 0;
  for (; $last_part < $parts_count; ++$last_part) {
    $length += strlen($parts[$last_part]);
    if ($length > $your_desired_width) { $parts[$last_part-1] .= "..."; break; }
  }
  
  return implode(array_slice($parts, 0, $last_part));
}




//************************************GET PLACE INFO FROM CACHE*****************************/

public static function get_google_place_cache($location, $radius, $type){
	//$request = self::places_request__.(($type == "supermarket")?"?query=":"?type=").$type."&rankby=prominence&language=en&radius=".$radius."&location=".$location."&key=".self::google_api_key__;	
	switch ($type) {
    	case 'shopping+mall':
        		$q = "?query=".$type."&rankby=distance&keyword=".$type;
        	break;
      case 'point_of_interest':
        		$q = "?type=".$type."&rankby=prominence&radius=".$radius;
        	break;
    	case 'supermarket':
        		$q = "?query=".$type."&rankby=distance&keyword=".$type;
        	break;
        case 'pub':
        		$q = "?query=".$type."&rankby=prominence&radius=".$radius;
        	break;
    	default:
        		$q = "?query=".$type."&rankby=prominence&radius=".$radius;
        	break;
	}

	$request = self::places_request__.$q."&language=en&location=".$location."&key=".self::google_api_key__;
	$code = "obc_google_".md5($location." ".$type);		
	//echo $request;
	if ( false === ( $googleData = get_transient( $code ) ) ) {	
		$j = @file_get_contents($request);
		$data = json_decode($j,true);
		$places = $data['results'];
		if(isset($places)){
			//echo $request;
			$count = count($places);	
			for($i = 0; $i < $count; $i++){				
					$place = $places[$i];				
					$final_array[$i]['name'] = $place['name'];
					$final_array[$i]['opening_hours'] = $place['opening_hours'];
					$final_array[$i]['types'] = $place['types']; 
					$final_array[$i]['rating'] = $place['rating'];
					$final_array[$i]['photos'] = $place['photos'];				
					$final_array[$i]['place_id'] = $place['place_id'];				
					$final_array[$i]['location'] = $place['geometry']['location'];
					$final_array[$i]['weekday_text'] = self::get_opening_hours_from_cache($place['place_id']);				
			}
			$googleData = $final_array;
		    set_transient( $code, $googleData, 30 * DAY_IN_SECONDS );		
		}
	}
  return $googleData;
}




public static function get_one_google_place_cache($place_id){
	$code = "obc_google_".md5($place_id);
	$place = get_transient( $code );
	return $place;
}





public static function get_opening_hours_from_cache($place_id){
 	$code_for_place = "obc_google_".md5($place_id);
 	if($googleData = get_transient( $code_for_place )){
		$num_day_of_week = date('N')-1;		
		if(isset($googleData['weekday_text'])){
				$hours = preg_replace("/\w*day:\s/", ": ", $googleData['weekday_text'][$num_day_of_week]);		
		}
		else{ 
				$hours = '';
		}
		return $hours;
	}
	return '';
}


/******************************************Offblock it! button callback ***************************/

function offblock_it_callback(){
	global $wpdb;
	check_ajax_referer( 'google_request_nonce', 'security' );
	if( ! wp_verify_nonce( $_POST['security'], 'google_request_nonce' ) ) die( 'REQUEST ERROR!');
	//$code = "obc_google_".md5($location." ".$type);
	$place_id = $_POST['place_id'];
	$code = "obc_google_".md5($place_id);
	$is_place = get_transient($code);
	if(isset($_POST['place_info']) && !$is_place){
		$googleData = $_POST['place_info'];
		set_transient( $code, $googleData, 30 * DAY_IN_SECONDS );			
	}	
	$place_id = $_POST['place_id'];
	echo self::is_post_pending($place_id);
	die();
}

function is_post_pending($place_id){
	global $wpdb;
	$is_pending = $wpdb->get_var( "SELECT $wpdb->posts.ID FROM $wpdb->posts LEFT JOIN $wpdb->postmeta ON $wpdb->posts.ID = $wpdb->postmeta.post_id 
										WHERE $wpdb->posts.post_status = 'draft' 
										AND $wpdb->posts.post_type = 'place' 
										AND $wpdb->postmeta.meta_key = 'place_id' 
										AND $wpdb->postmeta.meta_value = '".$place_id."';" );
	return $is_pending;
}


/**********************************GET RESULTS FOR EVERY TAB WITH DIFFERENT PLACE TYPES************************************/
function get_results_callback(){
	check_ajax_referer( 'ajax_get_results', 'security' );	
	if( ! wp_verify_nonce( $_POST['security'], 'ajax_get_results' ) ) die( 'REQUEST ERROR!');
	if(isset($_POST['post_id'])){$city_id = $_POST['post_id'];}
	switch ($_POST["title"]) {
			case 'Eating Out':
        	$types = "restaurant, cafe, brewpub, lebanese+restaurant";
        	$radius = "5000";
        	break;
    	case 'Going Out':
        	$types = "bar, pub, sports+bar";
        	$radius = "5000";
        	break;
    	case 'Shopping':
        	$types = "shopping+mall, store, cosmetics+store, electronics+store, department+store, drug+store";
        	$radius = "5000";
        	break;
		  case 'Supermarkets':
        	$types = "supermarket";
        	$radius = "15000";
        	break;
    	case 'Things to do':
        	$types = "sightseeing, point_of_interest, hop+on+hop+off, motorcycle+rental+agency, entertainment";
        	$radius = "2500";
        	break;
	}
	$atts = array( 'type'	=> $types,
				   'radius' => $radius,
				   'user_lat'=>$_POST["user_lat"],
				   'user_lng'=>$_POST["user_lng"],
				   'header' => $_POST["title"],
				   'city_id' => $city_id
		);		
	echo self::print_places($atts);//do_shortcode('[city_places type="pub, bar" radius="5000" header="Going Out"]');print_places	
	wp_die();

}



/*************************************DETAIL INFO ABOUT APPROVED PLACE****************************************************/

function show_approved_place_callback(){
	check_ajax_referer( 'ajax_nonce_app', 'security' );
	if( ! wp_verify_nonce( $_POST['security'], 'ajax_nonce_app' ) ) die( 'REQUEST ERROR!');
	//$code = "obc_google_".md5($location." ".$type);
	global $wpdb, $post;	

	$place_id = $_POST['place_id'];
	$app_place_html = $_POST['app_place_html'];	
	$place_from_google = self::get_one_google_place_cache($place_id);
  
	$place_db_id = $wpdb->get_var( "SELECT $wpdb->posts.ID FROM $wpdb->posts LEFT JOIN $wpdb->postmeta ON $wpdb->posts.ID = $wpdb->postmeta.post_id 
										WHERE $wpdb->posts.post_status = 'publish' 
										AND $wpdb->posts.post_type = 'place' 
										AND $wpdb->postmeta.meta_key = 'place_id' 
										AND $wpdb->postmeta.meta_value = '".$place_id."';" );	
	$place = get_fields($place_db_id);
	$name = esc_html(get_the_title($place_db_id));	
	$place_type_obj = get_field('sub_category',$place_db_id);
	$type = get_the_title($place_type_obj); 

	$images_from_db = get_field('gallery',$place_db_id);	
	$overview = get_field('overview', $place_db_id);
	/****************************COMMENTS*****************************/
	$approved_image = plugin_dir_url( __FILE__ )."views/images/approved.png";	

	$comments = get_comments(array(
			'post_id' => $place_db_id,
			'status' => 'approve' // APPROVED COMMENTS 
		));

		// FORMING LIST OF COMMENTS
	$comm_ents = wp_list_comments(array(
			'per_page' => 10, 
			'reverse_top_level' => false, // LAS COMMENTS IN THE BEGINNING
			'style'             => 'ul',
			'avatar_size'       => 0,
			'callback'          => 'offblocks_comment',
			'echo'              => false    
		), $comments);		
	$form = self::output_comment_form($place_db_id,$place_from_google['city_id']);
	$website = get_field('website',$place_db_id);	
	$website  = preg_replace('/https?:\/\//', "", $website);	
	$website  = preg_replace('/(\/.*\/)*/', "", $website);
	//$website  = preg_replace('/(\/\?.*)*/', "", $website);
	//echo count($images_from_db)."-".count($place_from_google['photos']);
	echo View::make('cities/approved_place.twig', ['place'=>$place, 
												   'images_from_db'=>$images_from_db, 
												   'type'=>$type, 
												   'place_from_google'=>$place_from_google, 
												   'title'=>$name,
												   'overview'=>$overview,
												   'website'=>$website,
												   'app_place_html'=>$app_place_html,
												   'approved_image'=>$approved_image,
												   'comments'=>$comm_ents,
												   'comment_form'=>$form], ['wpautop' => false]);	
	die();															   
}




/*********************************OUTPUT COMMENT FORM************************/


public static function output_comment_form($post_id,$city_id)
{
  global $post;  
  ob_start();  
  $city = get_field('city',$post_id);  
  $defaults = array(	
	'comment_field'        => '<p class="comment-form-comment"><label for="comment">' . _x( 'Comments', 'noun' ) . '</label> <textarea id="comment" name="comment" cols="45" rows="8"  aria-required="true" required="required"></textarea></p>',
	'must_log_in'          => '<p class="must-log-in">' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $city ) ) ) ) . '</p>',
	'logged_in_as'         => '',
	'comment_notes_before' => '',
	'comment_notes_after'  => '<p class="form-allowed-tags">All comments are approved by offblocks.com before being published</p>
							   <input name="redirect_to" type="hidden" value="'.get_permalink($city_id).'" />',
	'id_form'              => 'commentform',
	'id_submit'            => 'submit',
	'class_form'           => 'comment-form',
	'class_submit'         => 'submit',
	'name_submit'          => 'submit',
	'title_reply'          => '',
	'title_reply_to'       => __( 'Leave a Reply to %s' ),
	'title_reply_before'   => '',
	'title_reply_after'    => '',
	'cancel_reply_before'  => ' <small>',
	'cancel_reply_after'   => '</small>',
	'cancel_reply_link'    => __( 'Cancel reply' ),
	'label_submit'         => __( 'Comment' ),
	'submit_button'        => '<input name="%1$s" type="submit" id="%2$s" class="%3$s" value="%4$s" />',
	'submit_field'         => '<p class="form-submit">%1$s %2$s</p>',
	'format'               => 'xhtml',
);
  comment_form($defaults,$post_id);        
  $form = ob_get_clean();   
  return $form;
}



	
/******************************EDITING GOOGLE PLACE POST AND OUTPUT HIS FORM***************/

function edit_place_callback(){
	global $post;
	global $wpdb;
	check_ajax_referer( 'ajax_nonce_app', 'security' );
	if( ! wp_verify_nonce( $_POST['security'], 'ajax_nonce_app' ) ) die( 'REQUEST ERROR!');
	//$code = "obc_google_".md5($location." ".$type);
	$ref = get_permalink();
	$city = get_the_title();
	//echo $city;
	$is_pending = self::is_post_pending($_POST['place_id']);
	if($is_pending){
		//echo $is_pending;
		$place = self::get_one_google_place_cache($_POST['place_id']);	
		$url = home_url()."?post_type=place&action=edit&p=".$is_pending."&city_id=".$place['city_id'];		
	}
	if(isset($_POST['place_id']) && null == $is_pending){		
		$place = self::get_one_google_place_cache($_POST['place_id']);			
		$cur_user_id = get_current_user_id();

/*****************************POST CREATING*******************************************/
		$address = $place['name'].", ".$place['location']['address'];
		$capturedData = self::getLocationInformation($place['address_components']);				
		$post_data = array(
		 'post_title'    => $place['name'],
 		 'post_content'  => '',
 		 'post_status'   => 'draft',
  		 'post_author'   => $cur_user_id,
  		 'post_type' 	 => 'place',
  		 'post_category' => array( 8,39 ),
  		 'meta_input'  => [
							/*'country'    => $country->ID,
							'region'     => $region->ID,
							'airports' => [],*/
							'city' 		  => [],
							'sub_category'=> [],
							'google_map' => [
								'address' => $address,
								'lat'     => $place['location']['lat'],
								'lng'     => $place['location']['lng'],								
							],							
							'_google_map' => Load::getFieldName('google_map','place'),
							'_city' => Load::getFieldName('city','place'),
							'_sub_category' => Load::getFieldName('sub_category','place'),
						]
		);				
		$post_id = wp_insert_post( $post_data );
		
		update_field('phone_number', $place['international_phone_number'],$post_id);
		update_field('website',$place['website'],$post_id);
		//update_field('overview',$place['reviews'][0]['text'],$post_id);		
		
		update_field('rating',$place['rating'],$post_id);

		$short_addr = explode("-", $place['address']);
		update_field('address',$short_addr[0]." - ".$short_addr[1],$post_id);
		$type = $place['types'];

		$catid = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title = '" . $type[0]. "';" );
		if(!isset($catid)){
			for($i=1; $i<count($type);$i++){
				$catid = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title = '" . $type[$i]. "';" );
				if(isset($catid)){break;}
			}
		}		
		update_field('sub_category',$catid,$post_id);

		$city_field_name =  Load::getFieldName('city','place');
		$cityid = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title = '" . $capturedData['city'] . "' AND post_type='city'" );
		//update_field($city_field_name,array($cityid,),$post_id);
		update_field($city_field_name,$cityid,$post_id);

		update_field('place_id',$_POST['place_id'],$post_id);
		self::set_opening_times($place['weekday_text'],$post_id);

		$url = home_url()."?post_type=place&action=edit&p=".$post_id."&city_id=".$place['city_id'];//city_name;		
	}
	echo $url;
	die();
}

  
  
/**************************************  A D D I N G   P L A C E ************************************************/
function add_place_callback(){
	global $post;	
	check_ajax_referer( 'ajax_nonce_app', 'security' );
	$city_id = (isset($_POST['city']))?$_POST['city']:"";
	if( ! wp_verify_nonce( $_POST['security'], 'ajax_nonce_app' ) ) die( 'REQUEST ERROR!');
	if(isset($_POST['action']) && $_POST['action'] == "add_place"){		
		$cur_user_id = get_current_user_id();
		$post_data = array(
			'post_title'    => 'New place',
 		 	'post_content'  => '',
 		 	'post_status'   => 'draft',
  		 	'post_author'   => $cur_user_id,
  		 	'post_type' 	 => 'place',
  		 	//'post_category' => array( 8,39 ),
  		 	'meta_input'  => [							
							'city' 		  => [$post->ID],
              'place_id'  => uniqid('man_'),
							'sub_category'=> [],
							'google_map' => [],							
							'_google_map' => Load::getFieldName('google_map','place'),
							'_city' => Load::getFieldName('city','place'),
							'_sub_category' => Load::getFieldName('sub_category','place'),
						]
		);				
		$post_id = wp_insert_post( $post_data );
		$city_field_name =  Load::getFieldName('city','place');
		
		if(isset($post_id) && isset($city_field_name) && $city_id !="") update_field($city_field_name,$city_id,$post_id);
		$url = home_url()."?post_type=place&action=edit&p=".$post_id."&city_id=".$city_id;//city_name;		
		echo $url;
	}
	die();

}

  
  
public static function place_form() {
	global $post;
	global $wpdb;

	if (isset($_GET['p'])) { $post_id = $_GET['p']; }
	else {$post_id = "";}
		setup_postdata($post->ID);		
		$place_id = get_field( "place_id" );
		$place = self::get_one_google_place_cache($place_id);
	  $place['name'] = wp_unslash($place['name']);
		$cur_user_id = get_current_user_id();

		$city_id = get_field('city');
		if(isset($_GET['city_id'])){$city = get_post($_GET['city_id']);}	
		else { $city = get_post($city_id); } 
		$city_name = $city->post_name;
		// if there is no city in google places like in db, then use for ref global post name from db
		$ref = home_url()."/city/".$city_name."?place_updated=1";
		//$ref = get_permalink();
//*************************OUTPUT PLACE AND HIS FORM
		$weekday_text = self::get_opening_hours_from_cache($post_id);
		//echo View::make('cities/place_head.twig', ['place'=>$place, 'weekday_text'=>$weekday_text], ['wpautop' => false]);

		if(isset($_GET['action']) && $_GET['action']=='edit') {
			$fields = array('sub_category','google_map','address','city','opening_times','phone_number', 'website','overview','gallery');
			$args = array(
    			'post_id' => $post_id, 
					'post_title' => true,
    			'return' => $ref,
    			'instruction_placement' => 'field',                 
    			'html_after_fields' => '<input type="hidden" id="changed" name="changed" value="off">
									    <input type="hidden" id="changed_fields_array" name="changed_fields_array" value="">',
    			'fields'  => $fields,
    			'form' => true,
    			'updated_message' => __("Place has been sent to approval", 'acf'),
    			'html_updated_message'	=> '<div id="message" class="updated"><p>%s</p></div>',
    			'submit_value'  => 'Save & Send for Approval'
  			);		
			acf_form( $args );
		}
}



	
	public static function set_opening_times($opening_times, $post_id){
		$count = count($opening_times);
		$prev_time = '';
		$prev_weekday = '';
		// check if the repeater field has rows of data
		if( !have_rows('opening_times',$post_id) ){
 			// loop through the rows of data			
 			$result = array();
 			$kopening_times = Load::getFieldName('opening_times','place');
 			//forming array of weekdays opening time
    		for ($i=0; $i < $count; $i++) {				
    			$pieces = explode(": ", $opening_times[$i]);    			    			    			
    			$kweek_day = Load::getFieldName('opening_times','place','week_day');
    			$kopening_hours = Load::getFieldName('opening_times','place','opening_time');
    			
    			if($prev_time == $pieces[1] && $prev_weekday  != $pieces[0]){
    				$pieces[0] = $prev_weekday.",".$pieces[0];
    				$del = $i;    				
    				//Delete previous array element
    				array_splice($result, -1);
    			}
       		 	$row = array(
					$kweek_day	=> $pieces[0],
					$kopening_hours	=> $pieces[1],
				);		
				array_push($result,$row);				
				$prev_time = $pieces[1];
				$prev_weekday = $pieces[0];			
    		}    		    		
    		//Pushing array into repeater
    		$count = count($result);
    		
    		for ($i=0; $i < $count; $i++) {				
    			$pieces = explode(",", $result[$i][$kweek_day]);
    			$count_days =  count($pieces);
    			if($count_days>1){
    				$result[$i][$kweek_day] = $pieces[0]."-".$pieces[$count_days-1];
    			}
    			else{
    				$result[$i][$kweek_day] = $pieces[0];
    			}
    			$row_field = add_row($kopening_times, $result[$i], $post_id);
    		}			  
		}
	}



/**********************************GET WUNDERGROUND FORECAST***********************************/	

	public static function wunderground_hourly_forecast($request, $param){
		$code = "obc_wu_".md5($request);		
		$hourly_forecast = get_transient( $code );
		if (( false === $hourly_forecast ) || (empty($hourly_forecast))) {	
			echo $request;
			$j = @file_get_contents($request);
			$data = json_decode($j,true);			
			$hourly_forecast = $data['hourly_forecast'];			
			if(!empty($hourly_forecast)){				
				set_transient( $code, $hourly_forecast, HOUR_IN_SECONDS*6 );			
			}
		}
		
		$count = count($hourly_forecast);
		$chart_data = array();
		for($i = 0; $i < $count; $i++)
			{
 			 $forecast = $hourly_forecast[$i];  
 			 $time = $forecast['FCTTIME']['hour']." ".$forecast['FCTTIME']['ampm'];
 			 switch($param){
 				case 'temperature': $chart_data[$i]['FCTTIME'] = ((($i%3)==0)?$time:"");
 							 $chart_data[$i]['value'] = $forecast['temp']['metric'];
 							 break;
 				case 'wind speed': $chart_data[$i]['FCTTIME'] = ((($i%3)==0)?$time:"");
 							 $chart_data[$i]['value'] = $forecast['wspd']['metric'];
 							 break;
 				case 'precipitation' : $chart_data[$i]['FCTTIME'] = ((($i%3)==0)?$time:"");
 							 $chart_data[$i]['value'] = $forecast['pop'];
 							 break; 					  
  			 }
		}
		$chart = new \LineChart(450,200);
		$dataSet = new \XYDataSet();
		for($i=00; $i < 20; $i++){
			if(isset($chart_data[$i])){
				$dataSet->addPoint(new \Point($chart_data[$i]['FCTTIME'], $chart_data[$i]['value']));	
			}	
		}
		$chart->setDataSet($dataSet);
		$chart->setTitle("Forecast for ".$param." for next 24 hours");
		$chart->render(plugin_dir_path( __FILE__ )."generated/demo5.png");
		$chart_img =  "<img alt='Line chart' src='".plugin_dir_url( __FILE__ )."generated/demo5.png'/>"; 
		return  $chart_img;	
	}




public static function get_furl($url)
    {
    $furl = false;
    //echo $url;
    $code = "obc_google_photo_".md5($url);   		    		 

	if ( false === ( $furl = get_transient( $code ) ) ) {	
    	// First check response headers
   		 $headers = get_headers($url);
   		 // Test for 301 or 302 
   		 if(preg_match('/^HTTP\/\d\.\d\s+(301|302)/',$headers[0]))
      	  {
      		  foreach($headers as $value)
         	   {
            		if(substr(strtolower($value), 0, 9) == "location:")
              	  {
              		  $furl = trim(substr($value, 9, strlen($value)));
               	  }
           	   }
           	   //echo $furl;
           	   set_transient( $code, $furl, 30 * DAY_IN_SECONDS );
       	 }       	
    }
   		 // Set final URL
   	$furl = ($furl) ? $furl : $url;
    return $furl;
}



public static function my_acf_input_admin_footer() {
	if(!is_admin()){
		?>
		<script type="text/javascript">
			(function($) {
				acf.do_action('append', $('.acf-gallery-add'));	
				if( acf ) acf.do_action('prepare');
				// JS here
	
			})(jQuery);	
		</script>
		<?php
	}
}


public static function associatePlaceWithCategory($postID, $subcatID){
	$place = get_post_meta($postID, 'sub_category', true);
	if ($place != $subcatID) {
		update_post_meta($postID, 'sub_category', $place);
	}

}


public static function getLocationInformation($address_components)
	{
		$results = $address_components;
		$capturedData = [];

		foreach ($results as $result) {
			foreach ($result['types'] as $type) {
				if (array_key_exists($type, static::$addressComponentsMap)) {
					$capturedData[static::$addressComponentsMap[$type]] = $result['short_name'];
				}
			}
		}

		return $capturedData;

	}


public static function print_city_end($atts){		
	return View::make('airports/airport_the_end.twig', ['form'=>((isset($atts['form']))?$atts['form']:'')], ['wpautop' => false]);
}



public static function print_stay_safe(){
	global $post;
	$stay_safe = get_field('stay_safe');		
  $path_to_plugin = plugin_dir_url( __FILE__ );
	return View::make('cities/stay_safe.twig', ['stay_safe'=>$stay_safe,
                                              'path'     =>$path_to_plugin], ['wpautop' => false]);
}



public static function print_city_airports(){
		global $post;
		$airports_id = get_post_meta($post->ID, 'airports', true);
        $airports = array();
        $i=0;
        $google_map = get_field('google_map',$post->ID);
        $city_name = get_the_title();
        foreach ($airports_id as $post_id) {
        	$post = get_post($post_id);
        	setup_postdata( $post );
        	$airports[$i]['gallery'] = get_field('gallery');
        	$airports[$i]['title'] = get_the_title();        	
        	$airports[$i]['permalink'] = get_permalink();
        	$airports[$i]['iata'] = get_field('iata');
        	$airports[$i]['google_map'] = get_field('google_map');
        	if(!isset($airports[$i]['gallery'][0])){
        		$airport_name = str_replace(' ','%20',$airports[$i]['title']);
        		$meta = "https://maps.googleapis.com/maps/api/streetview/metadata?size=600x300&location=".$airport_name."&key=".self::google_api_key__;
        		$url = "https://maps.googleapis.com/maps/api/streetview?size=600x300&location=".$airport_name."&key=".self::google_api_key__;
        		$j = @file_get_contents($meta);
        		$metadata = json_decode($j, true);
			
 				if($metadata['status']=='OK'){    		
        			$airports[$i]['street_view_photo'] = self::get_furl($url);
        		}
        		else{
        			$path = plugin_dir_url( __FILE__ );
        			$airports[$i]['street_view_photo'] = $path.'views/images/airport.png';	
        		} 
        		       		
        	}
        	$i++;
        }
        wp_reset_postdata();
        $upload_dir = wp_upload_dir(); 
        //$upload_dir['baseurl'];
        $path_to_plugin = plugin_dir_url( __FILE__ );
        return View::make('cities/city_airports.twig', ['airports'=>$airports,
      													'upload_dir'=>$upload_dir,
      													'city_name'=>$city_name,      											
      													'google_map'=>$google_map,
                                'path'      =>$path_to_plugin], ['wpautop' => false]);
	}
}