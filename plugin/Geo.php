<?php

namespace OffBlocks;


use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;
use MaxMind\Db\Reader\InvalidDatabaseException;

class Geo
{

   static $settings = [
  'server'         => 'http://geolite.maxmind.com/download/geoip/database/',
  'zipFile'        => 'GeoLite2-City.mmdb.gz',
  'dataFile'       => 'GeoLite2-City.mmdb',
  'defaultCountry' => 'US',
 ];

  
  public static function Setup() { 
  
    //https://www.offblocks.com/wp-admin/admin-ajax.php?action=geo_update_database
    add_action('wp_ajax_nopriv_geo_update_database',__CLASS__.'::updateGeoDatabase');
    add_action('wp_ajax_geo_update_database',__CLASS__.'::updateGeoDatabase');


    //http://www.offblocks.com/wp-admin/admin-ajax.php?action=geo_get
    add_action('wp_ajax_nopriv_geo_get',__CLASS__.'::get');
    add_action('wp_ajax_geo_get',__CLASS__.'::get');
    
  }
  
  
  
  function get($ip= '')
 {

    
    if (!$ip) {
      $ip = $_SERVER['REMOTE_ADDR'];
    }
 

  $dataFile =self::folderName() . self::$settings['dataFile'];

  if (file_exists($dataFile)) {
    
    

   $reader = new Reader($dataFile);
   try {
    $data = $reader->city($ip);
   } catch (AddressNotFoundException $e) {
    return [
     'success' => false,
     'message' => 'IP address not found',
    ];

   } catch (InvalidDatabaseException $e) {
    return [
     'success' => false,
     'message' => 'A database error occured.',
    ];
   }

   
   $data =  [
    
    'success'      => true,
    'isocode'      => $data->country->isoCode,
    'country'      => $data->country->name,
    'city'         => $data->city->name,
    'latitude'     => $data->location->latitude,
    'longitude'    => $data->location->longitude,
    'ip'           => $ip,
   ];
    
    return $data;
  }
  return [];
    exit;
 }


 public static function folderName()
 {
   
   $upload_dir   = wp_upload_dir();
 
   return $upload_dir['basedir'].'/geo/';
 }

 public static function updateGeoDatabase()
 {
   
  set_time_limit(0);
   
  $filename = self::$settings['zipFile'];
  $toFilename = self::$settings['dataFile'];
  $server = self::$settings['server'];
   
   $folder = self::folderName();

  $from = $folder . $filename;
  $to = $folder . $toFilename;

   
   
  self::SaveRemoteFileLocally($server . $filename, $from);

  $sfp = gzopen($from, "rb");
  $fp = fopen($to, "w");

  while (!gzeof($sfp)) {
   $string = gzread($sfp, 4096);
   fwrite($fp, $string, strlen($string));
  }
  gzclose($sfp);
  fclose($fp);
  unlink($from);
 }

 public static function SaveRemoteFileLocally($remoteFile, $localFile)
 {
  $fp = fopen($localFile, 'w+'); //This is the file where we save the    information
  $ch = curl_init($remoteFile); //Here is the file we are downloading, replace spaces with %20
  curl_setopt($ch, CURLOPT_TIMEOUT, 50);
  curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  curl_exec($ch); // get curl response
  curl_close($ch);
  fclose($fp);
 }

  
}

