/******************* HTML 5 HISTORY WITH TABS****************/

function getTabnum($direction){
		$num = $('.x-nav-tabs-item.u-tab-class.active a').data('xToggleable');			 
		$num = ($direction=='back')?$num-1:$num+1;
		return $num;
}

function goback() {
    $('#x-legacy-tab-'+getTabnum('back')).click();
}
function goforward() {
    $('#x-legacy-tab-'+getTabnum('forward')).click();
}
