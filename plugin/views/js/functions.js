$google_api_key__ = "AIzaSyAF0N3AqZpy25gKyVPqayaEswt749qPJ4M";
$places_photo_request__ = "https://maps.googleapis.com/maps/api/place/photo";
$ = jQuery.noConflict();
$offset_top=0;
var app_place_html;
var PLACE_HEIGHT = 150;
var speed=500,
    originalHeight=PLACE_HEIGHT+10,
    expanededHeight=PLACE_HEIGHT+260;//originalHeight+250;      
var $pending_post_id = 0;
var $user_lat=0, $user_lng=0;
var $default_image =  "";
jQuery(document).ready(function () {     

  /*************************************HANDLER FUNCTION ON CLICK ON APPROVED PLACE BUTTONS*****************/
  $approved = $('.approved');
  if(typeof $approved !== 'undefined'){    
    $('.u-tab-class').on('click', '.read_more_button_approved', function approved_placeinfo(){        
        $button = $(this);
        $place  = $button.closest("div.google_place");
        $tab    = $place.closest("div.u-tab-class");
        $wrap   = $place.closest("div.entry-wrap");
        if($button.text() == 'Read more'){              
          Close_place("button:contains('Close')");                    
          //css_classes_button_change(this, 'card-approved-btn', 'card-close-btn');
          $destination = $place.offset().top-135;                        
          $("html, body").animate({ scrollTop: $destination }, 500);            
          var data = {
            action: 'show_approved_place',                                        
            place_id: $place.attr('id'),
            app_place_html: $place.html(),
            security: $ajax_nonce_app
          };
          app_place_html = $place.html();
          jQuery.post( myajax.url, data, function(response) {                             
              $place.html('');
              $place.prepend(response);
          });        
      }
      else if($button.text() == 'Close'){      	  
          $(this).text('Read more');          
          $place.html('');
          $place.html(app_place_html);         
      }        
    });  
  }
});

jQuery(document).ready(function () {     
  //$approved.on('click', '.read_more_button_approved', function approved_placeinfo(){  
  $('.u-tab-class').on('click', '.read_more_button', function() {
        $button = $(this);
        $place = $button.closest("div.google_place");
        $place_photo = $place.find("div.photo_of_place");
          var speed=1000,
            originalHeight=PLACE_HEIGHT+10,
              expanededHeight=PLACE_HEIGHT+260;         

        if($button.text() == 'Read more'){    
/*********************************OPEN PLACE*****************************/
            Close_place("button:contains('Close')");                    
            // Destination of the scroll to the top of current place div
            $destination = $place.offset().top-135;                        
            $("html, body").animate({ scrollTop: $destination }, 500);
            $place_name = $place.find("h5.pl_name").text();
            //Offset a place photo
           $place_photo.removeClass('pull-right');

/****************************OPENINNG HOURS AND CACHING RESULTS OF GOOGLE PLACE REQUEST THROUGH AJAX****/
            var map = new google.maps.Map(document.getElementById('map'), {
              center: {lat: $lat, lng: $lng },
              zoom: 15
            });
            var service = new google.maps.places.PlacesService(map);
            PlaceResult = service.getDetails({
              placeId: $place.attr('id'),
              language: 'en'
            }, function(place, status) {
              if (status === google.maps.places.PlacesServiceStatus.OK) {          
                place_tr = {'weekday_text':[], 'location':[],'international_phone_number':'','formatted_address':'','website':'','reviews':[],'place_id':'','name':'','types':[],'rating':'','address':'','city':'', 'address_components':[], 'photos':[]};            
                $working_times = $place.find('p#working_times');

                $num_day_of_week = get_day_of_week();
                if(typeof place.opening_hours !== 'undefined'){
                  for(i=0;i<7;i++){
                     $place_working_time = place.opening_hours.weekday_text[i];// REMAKE!!!
                     place.opening_hours.weekday_text[i] = $place_working_time.replace(/\w*day:\s/, ": " );
                     place_tr.weekday_text[i] = $place_working_time;      
                    }
                }
                else {
                  place_tr.weekday_text=[];
                }
                place_tr.location = {'lat': place.geometry.location.lat, 'lng': place.geometry.location.lng, 'address':place.formatted_address, 'zoom':20 };
                place_tr.international_phone_number = place.international_phone_number;
                place_tr.formatted_address = place.formatted_address;
                place_tr.website = place.website;
                
                if(typeof place.photos !== 'undefined'){
                  for (var i = 0; i < place.photos.length; i++) {
                    place_tr.photos[i] = place.photos[i].getUrl({'maxWidth': 400, 'maxHeight': 350});
                  }
                }

                place_tr.reviews = place.reviews;
                place_tr.place_id = place.place_id;
                place_tr.city_id = $city_id;
                place_tr.name = place.name;
                place_tr.types = place.types;
                place_tr.rating = place.rating;
                place_tr.address_components = place.address_components;
                place_tr.address = place.formatted_address;
/****************************SEND REQUEST FOR GOOGLE API PLACES FOR OPENING HOURS*********************/
                var data = {
                      action: 'offblock_it',
                      place_info: place_tr,
                      security: $ajax_nonce,                     
                      place_id: $place.attr('id')
                };
                jQuery.post( myajax.url, data, function(response) {
                       $pending_post_id = response;                                                                           
                });
                if(typeof place.opening_hours !== 'undefined'){
                  $res = place.opening_hours.weekday_text[$num_day_of_week];             
                  if($place[0].classList.contains("cached")==false){
                    $working_times.append($res);
                  }
                }
              }
          });

/**********************************OPENINNG HOURS********************************/
          
          //Append a text
          if($logged_in['log']){
            $offblock_button_text = (($place.data('pending'))?"<p id='pendtext' class='text-info'>Pending Edit Under Review</p>":
                                     "<button id='off_button' class='button_offblock center-block'>Offblock it !</button>");
            $place.append( $( "<p id='editing_place'>Have you been to "+$place_name+"? Do you want to leave a comment?</p>") );
            $place.append( $( "<div class='col-md-12 text-center text-info'>" + $offblock_button_text + "</div>"));
          }
            ajax_edit_place();
          //<button id='off_button' class='button_offblock center-block'>Offblock it!</button>
          //var pdiv = $place.add( "button" );
          $place.stop().animate({height:expanededHeight},speed);
          //Change button label
          css_classes_button_change(this, 'card-read-more-btn', 'card-close-btn');          
          $button.text('Close');
      }
/*********************************END OF OPEN PLACE EVENT FUNCTION ***************/
      else if($button.text() == 'Close')     {
        $(this).text('Read more');
          css_classes_button_change(this, 'card-close-btn', 'card-read-more-btn');        
          //offset a place photo
          $place_photo.addClass('pull-right');            
          // Remove a text
          $p = $place.find("#editing_place");
          $p.remove();
         // Remove an offblock it! button
          $offblock_button = $place.find("#off_button, #pendtext");                
          $offblock_button.remove();                    
          // Animate to original Height
          $place.stop().animate({height:originalHeight},speed);
          
      }         
      //return false;
     }); 
 });



function Close_place(but_id){  
      $button_to_close = $(but_id);            
      $place_to_close = $button_to_close.closest("div.google_place");
      $place_photo_to_close = $place_to_close.find("div.photo_of_place");               
      //If we need to close an approved place
      if($button_to_close.hasClass( "read_more_button_approved" )){ 
          $place_to_close.html('');
          $place_to_close.html(app_place_html);
          css_classes_button_change(but_id, 'card-close-btn', 'card-approved-btn');                    
      }
      else {   
      // If we need to close common place from google      
      	  css_classes_button_change(but_id, 'card-close-btn', 'card-read-more-btn');              
          $place_photo_to_close.addClass('pull-right');            
          // Remove a text
          $p_to_close = $place_to_close.find("#editing_place");
          $p_to_close.remove();
          // Remove an offblock it! button
          $offblock_button_to_close = $place_to_close.find("#off_button, #pendtext");                
          $offblock_button_to_close.remove();  
          $button_to_close.removeAttr('id');
          // Animate to original Height
          $place_to_close.height(originalHeight);                    
      }
      $button_to_close.text('Read more');
 }

function css_classes_button_change(but_id, $from, $to){
    $button = $(but_id);
    if($button.hasClass($from)) { 
            $button.removeClass($from);
            $button.addClass($to);
    }
}


/**********************************LINK OFFBLOCK IT! BUTTON CLICK FUNCTION TO EVENT*********/
function ajax_edit_place(){
      $ = jQuery.noConflict();
      $('#off_button').click( function () {
          $button = $(this);
          $place  = $button.closest("div.google_place");
          $tab    = $place.closest("div.u-tab-class");
          $wrap   = $place.closest("div.entry-wrap");
          var data = {
              action: 'edit_place',                                        
              place_id: $place.attr('id'),
              place_type: $place_type,
              security: $ajax_nonce_app
              };
              jQuery.post( myajax.url, data, function(response) {                 
                  window.location.replace(response);        
              });
      });  
}



jQuery(document).ready(function () {   
    $('.x-tab-content').on('click', '#addplace', function () {          
          var data = {
              action: 'add_place',                                                      
              security: $ajax_nonce_app,
              city: $city_id
              };
              jQuery.post( myajax.url, data, function(response) {                                 
                  window.location.replace(response);        
              });
      });
  });

function get_day_of_week(){
  var now = new Date();/*0-Sunday 1-Monday 2-Tuesday 3-Wednesday 4-Thursday 5-Friday 6-Saturday /
                       /*6-Sunday 0-Monday 1-Tuesday 2-Wednesday 3-Thursday 4-Friday 5-Saturday */
  $num_day_of_week = now.getDay();            
  if($num_day_of_week === 0){$num_day_of_week+=6;}
  else{$num_day_of_week -= 1;}
  return $num_day_of_week;
}




/*******************************************************************************************************/
/*************************************HANDLER FUNCTION FOR GET GOOGLE PLACE RESULTS FROM CACHE**********/
jQuery(document).ready(function () {     
$tab_clicked = ['Quick Facts','Stay safe','Airports'];
  $("li.u-tab-class a").click( function () {          
      $tab = $(this);
      $text = $tab.text();
      switch ($text) {
        case 'Eating Out':
            $default_image = 'eating.jpg';
            break;
        case 'Going Out':
            $default_image = 'going.jpg';
            break;
        case 'Shopping':
            $default_image = 'shopping.jpg';
            break;
        case 'Supermarkets':
            $default_image = 'supermarkets.jpg';
            break;
        case 'Things to do':
            $default_image = 'things-to-do.jpg';
            break;
        default:
            $default_image = '';
      }
      var data = {
              action: 'get_results',                                                      
              title: $text,
              user_lat: $user_lat,
              user_lng: $user_lng,
              security: $ajax_get_results,
              post_id : $post_id
              };                    
      if(!$tab_clicked.includes($text)){   
            /*show loading gif image if tab_content is zero */
            $tab_num = $(this).data('xToggleable');   
            $tab_cont = $("#x-legacy-panel-"+$tab_num).html();
            if($tab_cont ==="") { $( "#loading" ).show(); } 

          jQuery.post( myajax.url, data, function(response) {  
               
              $tab_content = $("div.active");                  
              $iempty = $tab_content.html();
              if($iempty === "") {
                  $( "#loading" ).show();
                  $tab_content.html(response);
              }
              $( "#loading" ).hide();
              /******* SHOW THE FIRST TWO PLACES WITH PICTURES *****/
              $('.active .photo_of_place').each(function() {
                  var place_id = $(this).attr('id');
                  if ($(this).isInViewport()) {
                      $img = $(this).find("img").first();
                      if($img.attr('src') === "" && place_id !== ''){ 
                          $img.attr('src', place_id); }
                      else if($img.attr('src') === "" && place_id === ''){
													$img.attr('src',$path+'views/images/'+$default_image);
                      }
                  }
              });
              /*****************************************************/      
      
       });
          $tab_clicked.push($text);
					//$img = $('.photo_of_place');				
      }
                    
  });

});


/******************************************SHOW PICTURE WHEN IT IS IN VIEWPORT************************************************/
$.fn.isInViewport = function() {
  var elementTop = $(this).offset().top;
  var elementBottom = elementTop + $(this).outerHeight();

  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();

  return elementBottom > viewportTop && elementTop < viewportBottom;
};


$(window).on('resize scroll', function() {
  $('.photo_of_place').each(function() {
      var place_id = $(this).attr('id');
    if ($(this).isInViewport()) {
      $img = $(this).find("img");
      if($img.attr('src') === "") {
				if(place_id !== ''){
					$img.attr('src',place_id);
				}
				else{
          $img.attr('src',$path+'/views/images/'+$default_image);
        }
			}
    } else {
      //$('#fixed-' + activeColor).removeClass(activeColor + '-active');
    }
  });
});
/**************************  G E O L O C A T I O N ****************/

$(document).on('ready', function(){
  var x = $("#demo");
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(usePosition);            
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }


 function usePosition(position) {
  $user_lat = position.coords['latitude'];
  $user_lng = position.coords['longitude'];
}

});
/************************C O M M E N T S ***************************/	

$(document).on('ready', function(){
    jQuery('#commentform input')
      .css({border: '1px solid #ccc', padding: '5px'});
    jQuery('#commentform textarea')
      .css({border: '1px solid #ccc', padding: '5px'});

  $(document).on('submit', '#commentform', function(){    
    var formData = jQuery("#commentform").serialize();

    jQuery.ajax({
      type: "POST",        
      url: myajax.comment_url,        
      data: formData,
     // security: $ajax_nonce,
      success: function(){
        jQuery('#respond').prepend('<div class="message"></div>');
        jQuery('#respond .message')
        .html("<div class='message_box'><b>Thank you.</b><span><i>Your comment may be pending moderation.</i></span></div>")
      .hide()
      .fadeIn(2000);
      setTimeout(function() { jQuery('#respond .message').fadeOut(1000); }, 5000);
      $('#comment').val('');
      },
      error: function(){
        
      }
    });      
    return false;
    var comment = jQuery('textarea#comment').val();
  });
});
