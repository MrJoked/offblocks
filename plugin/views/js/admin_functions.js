$ = jQuery.noConflict();

$.fn.post_change_visibility = function(e) {
    var $link_action = $(this);
    e.preventDefault();      		
    $action_to_post = (($link_action.hasClass('hide_place'))?'hide_place':(($link_action.hasClass('show_place'))?'show_place':''));
    var ajaxdata = {
	      action     : 'offblocks_hide_place',
	      act 		 : $action_to_post,
          post_id    : $(this).data('postid'),
	      nonce_code : hideajax.nonce
      };
    jQuery.post( ajaxurl, ajaxdata, function(response, event) {                             
              if(response){              	
                $link_action.removeClass($action_to_post);
              	$class_for_row = (($action_to_post=='hide_place')?'hidden_place':(($action_to_post=='show_place')?'place_to_show':''));              	              

                $action_to_post = (($action_to_post=='hide_place')?'show_place':(($action_to_post=='show_place')?'hide_place':''));             	
                $link_action.addClass($action_to_post);
                $place_button_text = (($action_to_post=='hide_place')?'Hide':(($action_to_post=='show_place')?'Show':'Hide')); 
              	if($link_action.closest( "tr" ).hasClass('hidden_place') && $class_for_row == 'place_to_show'){
              		$link_action.closest( "tr" ).removeClass('hidden_place');
              	}
              	$link_action.closest( "tr" ).addClass($class_for_row);
              	$link_action.html($place_button_text); 	
              }              
          });    		
};

$(document).ready(function(){
	$('.hide_place').click(function(e){
		$(this).post_change_visibility(e);
	});
	$('.show_place').click(function(e){
		$(this).post_change_visibility(e);
	});
	$( ".show_place" ).each(function( index ) {
		$(this).html('Show');  		
	});
});