<?php 
namespace OffBlocks;

class Notams{
  
  const notams_api_key__ = "149c69a0-0c1d-11e8-b3bf-634332eec99c";
  const notams_req_str__ = "https://v4p4sz5ijk.execute-api.us-east-1.amazonaws.com/anbdata/states/notams/notams-list";

	public static function Setup()
  {

  }
 
  public static function get_notams(){
    global $post;
    $request = self::notams_req_str__."format=json&type=airport&locations=OMDB&api_key=".self::notams_api_key__;	
	  $code = "obc_notams_".md5($request);
		if ( false === ( $data = get_transient( $code ) ) ) {	
		  	$j = @file_get_contents($request);
			  $data = json_decode($j,true);
			  set_transient( $code, $data, 3*HOUR_IN_SECONDS );		
		}
    for($i=0; $i<count($data);$i++){
      $notams_all[] = str_replace("/n", "</br>", $data[$i]['all']); 
    }
    return $notams_all;
}
}
